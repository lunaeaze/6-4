<?php

/*
 ==================================================================


カスタム投稿タイプ「商品詳細」の追加
及び、関する追加情報の定義

 */
// カスタム投稿タイプの追加
/*
 * 商品情報を追加するカスタム投稿項目の追加
 */
add_action("init", "create_product_contribution");
define("COURSE_CATEGORY_NAME", "course");
define("COURSE_DB_KEY", "course_detail_key");

define("COURSE_FREE_DATA", "c_free");
define("COURSE_FREE_DATA_TITLE", "c_free_title");
define("COURSE_FREE_DATA_CONTENT", "c_free_content");

define("COURSE_FREE_UNDER_DATA", "c_u_free");
define("COURSE_FREE_UNDER_DATA_TITLE", "c_u_free_title");
define("COURSE_FREE_UNDER_DATA_CONTENT", "c_u_free_content");
function create_product_contribution()
{
    $category_name = COURSE_CATEGORY_NAME;
    register_post_type($category_name, // 投稿タイプ名の定義
        array(
            "labels" => array(
                "name" => __("店舗コース"), // 表示する投稿タイプ名
                "singular_name" => __("店舗コース詳細")
            ),
            'supports' => array('title', 'thumbnail'),
            "public" => true,
            "menu_position" => 5,
            "has_archive" => true
        )
    );
//    register_taxonomy(
//        'product_type_taxonomy',
//        'product',
//        array(
//            'label' => '商品カテゴリ',
//            'labels' => array(
//                'all_items' => '商品カテゴリの追加',
//                'add_new_item' => '追加'
//            ),
//            'hierarchical' => true
//        )
//    );
    $product_slug = 'product';
    add_rewrite_rule("{$product_slug}/?$", "index.php?post_type={$product_slug}", 'top');
    add_rewrite_rule("{$product_slug}/([^/]+)/?$", "index.php?post_type={$product_slug}" . '&taxonomy=$matches[1]', 'top');
    /*
    add_rewrite_rule("{$product_slug}/{$feeds}/?$", "index.php?post_type={$post_type}".'&feed=$matches[1]', 'top');
    add_rewrite_rule("{$product_slug}/{$wp_rewrite->pagination_base}/([0-9]{1,})/?$", "index.php?post_type={$post_type}".'&paged=$matches[1]', 'top');
    */


}

//
//function change_posts_per_page_product($query)
//{
//    if (is_admin() || !$query->is_main_query())
//        return;
//    if ($query->is_archive('product')) {
//        $query->set('posts_per_page', '100');
//    }
//}
//
//add_action('pre_get_posts', 'change_posts_per_page_product');
//

function create_course_add_form_list()
{
    // 補足説明
    add_meta_box('course_explanation', 'コース内容追加する説明（上部）', 'course_explanation', COURSE_CATEGORY_NAME, 'normal', 'high');
    // コースの内容
    add_meta_box('course_detail', 'コース内容', 'course_detail', COURSE_CATEGORY_NAME, 'normal', 'high');

    // 補足説明
    add_meta_box('course_under_explanation', 'コース内容追加する説明（下部）', 'course_under_explanation', COURSE_CATEGORY_NAME, 'normal', 'high');
}

add_action('add_meta_boxes', 'create_course_add_form_list');

define("COURSE_TITLE", "c_title");
define("COURSE_SUB_TITLE", "c_subtitle");
define("COURSE_IMAGE", "c_image");
define("COURSE_PRICE", "c_price");
define("COURSE_ARTICLES_COUNT", "c_a_count");
define("COURSE_HUM_COUNT", "c_h_count");
define("COURSE_PLAN_DETAIL", "c_p_detail");
define("COURSE_DETAIL", "c_detail");
define("COURSE_MENU", "c_menu");
define("COURSE_DELIVERY_TIME", "c_d_time");
define("COURSE_ACTIVATE", "c_act");
/**
 * 仕様の追加フォーム
 */
function course_detail()
{
    global $post;
    wp_nonce_field('course_detail', 'course_detail_nonce');
    $contentList = get_post_meta($post->ID, COURSE_DB_KEY);

    if (is_array($contentList) && !empty($contentList)) {
        $contentList = $contentList[0];
    }
    $count = 0;
    if (is_array($contentList)) {
        $count = count($contentList);
    }

    ?>
    <div>

        <div>
            <button id="add_course">コース追加</button>
        </div>
        <div>
            <div>
                <h3>
                    コース編集フィールド(全てHTMLタグが利用できます)<span>+</span>
                </h3>
                <input type="hidden" name="max_idx" id="next_idx" value="<?php echo $count ?>">
                <div class="course_update_field">
                    <input type="hidden" name="update_target_idx" id="update_idx" value="<?php echo $count ?>">
                    <input type="hidden" name="course_update_type" value="add">
                    <div>表示する：<input type="checkbox" name="<?php echo COURSE_ACTIVATE ?>" value='active'></div>
                    <div class="content">
                        <input type="text" width="100%" placeholder="コースタイトル" name="<?php echo COURSE_TITLE ?>">
                    </div>
                    <div class="content">
                        <div class="left">
                            <div class="image_wrapper">
                                <img id="update_img">
                                <input type="hidden" id="update_img_val" name="<?php echo COURSE_IMAGE ?>"/>
                            </div>
                            <button id="update_image_load" type="button">画像を選択する</button>
                        </div>
                        <div class="right">

                            <div>
                                <input type="text" width="100%" placeholder="コースサブタイトル"
                                       name="<?php echo COURSE_SUB_TITLE ?>">
                            </div>
                            <div>
                                <input type="text" width="100%" placeholder="価格" name="<?php echo COURSE_PRICE ?>">
                            </div>
                            <div>
                                <input type="text" width="49%" placeholder="料理数"
                                       name="<?php echo COURSE_ARTICLES_COUNT ?>"> <input type="text" width="49%"
                                                                                          placeholder="人数"
                                                                                          name="<?php echo COURSE_HUM_COUNT ?>">
                            </div>
                            <div>
                                <input type="text" width="100%" placeholder="提供時間"
                                       name="<?php echo COURSE_DELIVERY_TIME ?>">
                            </div>
                        </div>
                    </div>
                    <div>
                        <textarea placeholder="メニュー" name="<?php echo COURSE_MENU ?>"></textarea>
                    </div>
                    <div>
                        <textarea placeholder="コース説明" name="<?php echo COURSE_DETAIL ?>"></textarea>
                    </div>
                    <div>
                        <textarea placeholder="プラン詳細" name="<?php echo COURSE_PLAN_DETAIL ?>"></textarea>
                    </div>
                    <div class="add_field">
                        <input class="add_btn" type="submit" value="更新する">
                    </div>
                </div>
            </div>
        </div>
        <div>
            <h3>登録コース一覧</h3>
            現在 <span><?php echo $count ?></span> 件のコースが登録されています
        </div>
        <div class="course_field_wrapper">
            <table id="course_table">
                <thead>
                <?php echo create_course_header(); ?>
                </thead>
                <tbody>
                <?php
                echo create_course_sample();
                for ($createCount = 0; $createCount < count($contentList); $createCount++) {
                    $contentMap = $contentList[$createCount];
                    echo create_course_field_row($contentMap, ["data-create-count" => $createCount, "class" => 'course_row']);
                }
                ?>
                </tbody>
            </table>
        </div>

    </div>
    <?php
}


/**
 * @return string
 */
function create_course_header()
{
    $field = "<tr>";
    $field .= "<th>表示/非表示</th>";
    $field .= "<th>画像</th>";
    $field .= "<th>タイトル</th>";
    $field .= "<th>削除</th>";
    return $field . "</tr>";
}

function create_course_sample()
{
    $attr = ['class' => "base_row"];
    return "<tr " . attr_data($attr) . ">" . create_course_field([], $attr) . "</tr>";
}

/**
 * @param $data
 * @param $row_attr
 * @return string
 */
function create_course_field($data = [], $row_attr = [])
{

    $field = "";
    $check = get_course_data($data, COURSE_ACTIVATE);
    if ($check) $check = "checked"; else $check = "";
    $field .= "<td><input type='checkbox' $check class='_active_ck' disabled='disabled'> </td>";
    // 画像
    $image_path = get_course_data($data, COURSE_IMAGE);
    $field .= "<td><img class='thumbnail_img' src='" . $image_path . "'/>" . "<input type='hidden' name='" . COURSE_IMAGE . "_[]' value='" . $image_path . "'>" . "</td>";

    // タイトル
    $title = get_course_data($data, COURSE_TITLE);
    $field .= "<td><p>" . $title . "</p>" . "<input type='hidden' name='" . COURSE_TITLE . "_[]' value='" . $title . "'>" . "</td>";

    // サブタイトル
    $sub_title = get_course_data($data, COURSE_SUB_TITLE);
    $field .= "<input type='hidden' name='" . COURSE_SUB_TITLE . "_[]' value='" . $sub_title . "'>" . "";
    // 価格
    $price = get_course_data($data, COURSE_PRICE);
    $field .= "<input type='hidden' name='" . COURSE_PRICE . "_[]' value='" . $price . "'>" . "";

    // 料理数
    $articles_count = get_course_data($data, COURSE_ARTICLES_COUNT);
    $field .= "<input type='hidden' name='" . COURSE_ARTICLES_COUNT . "_[]' value='" . $articles_count . "'>" . "";

    // 人数
    $hum_count = get_course_data($data, COURSE_HUM_COUNT);
    $field .= "<input type='hidden' name='" . COURSE_HUM_COUNT . "_[]' value='" . $hum_count . "'>" . "";

    // プラン詳細
    $plan_detail = get_course_data($data, COURSE_PLAN_DETAIL);
    $field .= "<input type='hidden' name='" . COURSE_PLAN_DETAIL . "_[]' value='" . $plan_detail . "'>";

    // コース内容
    $detail = get_course_data($data, COURSE_DETAIL);
    $field .= "<input type='hidden' name='" . COURSE_DETAIL . "_[]'  value='" . $detail . "'>";


    // コースメニュー
    $menu = get_course_data($data, COURSE_MENU);
    $field .= "<input type='hidden' name='" . COURSE_MENU . "_[]'  value='" . $menu . "'>";

    // 提供時間
    $delivery_time = get_course_data($data, COURSE_DELIVERY_TIME);
    $field .= "<input type='hidden' name='" . COURSE_DELIVERY_TIME . "_[]'  value='" . $delivery_time . "'></td>";
    $field .= "<td><input type='submit' value='削除' name='del'> </td>";


    return $field;
}

/**
 * @param array $row_attr
 * @return string
 */
function attr_data($row_attr = [])
{
    $attr = "";
    foreach ($row_attr as $key => $val) {
        $attr .= "$key='$val' ";
    }
    return $attr;
}

function create_course_field_row($data = [], $attr = [])
{
    return "<tr " . attr_data($attr) . ">" . create_course_field($data, $attr) . "</tr>";
}

/**
 * @param array $data
 * @param $key
 * @return mixed|string
 */
function get_course_data($data = [], $key)
{
    if (isset($data[$key])) return $data[$key];
    return "";
}

/**
 * 製品の画像登録用
 * @param $post
 */
function course_explanation($post)
{
    // nonce セキュリティ
    wp_nonce_field(wp_create_nonce(__FILE__), 'course_explanation_nonce');
    // カスタムフィールドの登録済の値を取得
    $free_data = get_post_meta($post->ID, COURSE_FREE_DATA);
    if (empty($free_data)) {
        $free_data = get_default_course_free_data();
    }


    if (is_array($free_data) && !empty($free_data)) {
        $free_data = $free_data[0];
    }
    $html = "<p>タイトル</p>";
    $title = $free_data[COURSE_FREE_DATA_TITLE];
    $html .= "<input type='text' name='" . COURSE_FREE_DATA_TITLE . "' value='$title'>";
    $html .= "<p>内容</p>";
    $content = $free_data[COURSE_FREE_DATA_CONTENT];
    $html .= "<textarea  name='" . COURSE_FREE_DATA_CONTENT . "'>$content</textarea>";
    echo $html;
}

/**
 * 製品の画像登録用
 * @param $post
 */
function course_under_explanation($post)
{
    // nonce セキュリティ
    wp_nonce_field(wp_create_nonce(__FILE__), 'course_under_explanation_nonce');
    // カスタムフィールドの登録済の値を取得
    $free_data = get_post_meta($post->ID, COURSE_FREE_UNDER_DATA);
    if (empty($free_data)) {
        $free_data = get_default_course_free_data();
    }


    if (is_array($free_data) && !empty($free_data)) {
        $free_data = $free_data[0];
    }
    $html = "<p>タイトル</p>";
    $title = $free_data[COURSE_FREE_UNDER_DATA_TITLE];
    $html .= "<input type='text' name='" . COURSE_FREE_UNDER_DATA_TITLE . "' value='$title'>";
    $html .= "<p>内容</p>";
    $content = $free_data[COURSE_FREE_UNDER_DATA_CONTENT];
    $html .= "<textarea  name='" . COURSE_FREE_UNDER_DATA_CONTENT . "'>$content</textarea>";
    echo $html;
}


/**
 * @return array
 */
function get_default_course_free_data()
{
    $data = [];
    $data[COURSE_FREE_DATA_TITLE] = "";
    $data[COURSE_FREE_DATA_CONTENT] = "";
    return $data;
}

/**
 * 登録関係の処理まとめ
 */
function create_product_add_save_list()
{

    add_action('save_post', 'specification_save');
    add_action('save_post', 'course_detail_save');
    add_action('save_post', 'specification_under_save');

}

create_product_add_save_list();

/**
 * 仕様の登録処理
 * @param $post_id
 * @return mixed
 */
function specification_save($post_id)
{
    global $post;
    $my_nonce = isset($_POST['course_explanation_nonce']) ? $_POST['course_explanation_nonce'] : null;
    if (!wp_verify_nonce($my_nonce, wp_create_nonce(__FILE__))) {
        return $post_id;
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }
    if (!current_user_can('edit_post', $post->ID)) {
        return $post_id;
    }

    if ($_POST['post_type'] == COURSE_CATEGORY_NAME) {
        $contentList = get_default_course_free_data();
        $contentList[COURSE_FREE_DATA_TITLE] = $_POST[COURSE_FREE_DATA_TITLE];
        $contentList[COURSE_FREE_DATA_CONTENT] = $_POST[COURSE_FREE_DATA_CONTENT];
        update_post_meta($post->ID, COURSE_FREE_DATA, $contentList);
    }
}

/**
 * 仕様の登録処理
 * @param $post_id
 * @return mixed
 */
function specification_under_save($post_id)
{
    global $post;
    $my_nonce = isset($_POST['course_under_explanation_nonce']) ? $_POST['course_under_explanation_nonce'] : null;
    if (!wp_verify_nonce($my_nonce, wp_create_nonce(__FILE__))) {
        return $post_id;
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }
    if (!current_user_can('edit_post', $post->ID)) {
        return $post_id;
    }

    if ($_POST['post_type'] == COURSE_CATEGORY_NAME) {
        $contentList = get_default_course_free_data();
        $contentList[COURSE_FREE_UNDER_DATA_TITLE] = $_POST[COURSE_FREE_UNDER_DATA_TITLE];
        $contentList[COURSE_FREE_UNDER_DATA_CONTENT] = $_POST[COURSE_FREE_UNDER_DATA_CONTENT];
        update_post_meta($post->ID, COURSE_FREE_UNDER_DATA, $contentList);
    }
}

/**
 * フォーム保存処理
 */
function course_detail_save($post_id)
{
    if (isset($_POST['course_detail_nonce']) && !wp_verify_nonce($_POST['course_detail_nonce'], 'course_detail')) {
        return $post_id;
    }

//    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
//        return $post_id;
//    }
    if (isset($_POST['post_type']) && COURSE_CATEGORY_NAME == $_POST['post_type']) {
        if (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }
    } else {
        return $post_id;
    }
    $add_data = get_post_meta($post_id, COURSE_DB_KEY);

    if (!is_array($add_data)) {
        $add_data = [];
    } else if (isset($add_data[0])) {
        $add_data = $add_data[0];
    }

    $is_del = $_POST["course_update_type"];
    $data=[];
    $is_update = false;
    if ($is_del === "del") {
        $update = [];
        $skip_idx = $_POST["update_target_idx"] - 0;
        for ($i = 0, $len = count($add_data); $i < $len; $i++) {
            if ($skip_idx == $i) {
                continue;
            }
            $update[] = $add_data[$i];
        }
        $add_data = $update;
        $is_update = true;
    } else {
        $index = $_POST["update_target_idx"];
        if (empty($index)) {
            $index = 0;
        }
        $data = get_default_course_data();
        $data[COURSE_TITLE] = $_POST[COURSE_TITLE];
        $data[COURSE_SUB_TITLE] = $_POST[COURSE_SUB_TITLE];
        $data[COURSE_IMAGE] = $_POST[COURSE_IMAGE];
        $data[COURSE_PRICE] = $_POST[COURSE_PRICE];
        $data[COURSE_ARTICLES_COUNT] = $_POST[COURSE_ARTICLES_COUNT];
        $data[COURSE_HUM_COUNT] = $_POST[COURSE_HUM_COUNT];
        $data[COURSE_PLAN_DETAIL] = $_POST[COURSE_PLAN_DETAIL];
        $data[COURSE_DETAIL] = $_POST[COURSE_DETAIL];
        $data[COURSE_MENU] = $_POST[COURSE_MENU];
        $data[COURSE_DELIVERY_TIME] = $_POST[COURSE_DELIVERY_TIME];
        $data[COURSE_ACTIVATE] = $_POST[COURSE_ACTIVATE];
        $add_data[$index] = $data;
    }


    // もしすべての入力に入力が無かったら何もしない
    if(!empty($_POST[COURSE_TITLE]) || $is_update){
        update_post_meta($post_id, COURSE_DB_KEY, $add_data);
    }

}

/**
 * @return array
 */
function get_default_course_data()
{
    $data = [];
    $data[COURSE_TITLE] = "";
    $data[COURSE_SUB_TITLE] = "";
    $data[COURSE_IMAGE] = "";
    $data[COURSE_PRICE] = "";
    $data[COURSE_ARTICLES_COUNT] = "";
    $data[COURSE_HUM_COUNT] = "";
    $data[COURSE_PLAN_DETAIL] = "";
    $data[COURSE_DETAIL] = "";
    $data[COURSE_MENU] = "";
    $data[COURSE_DELIVERY_TIME] = "";
    return $data;
}

/**
 * @return bool
 */
function check_course_data_lz($data=[])
{
   return isset($data[COURSE_TITLE]) &&isset($data[COURSE_SUB_TITLE]) &&    isset($data[COURSE_IMAGE]) &&    isset($data[COURSE_PRICE]) &&    isset($data[COURSE_ARTICLES_COUNT]) &&    isset($data[COURSE_HUM_COUNT]) &&    isset($data[COURSE_PLAN_DETAIL]) &&    isset($data[COURSE_DETAIL]) &&    isset($data[COURSE_MENU]) &&    isset($data[COURSE_DELIVERY_TIME]);
}




/*
 ==================================================================
 */

