<?php
/**
 * Created by PhpStorm.
 * User: Serinappa
 * Date: 2018/12/04
 * Time: 20:11
 */
// コースをロードする
/**
 * @var array
 */
$course_list = get_post_meta(get_the_ID(), COURSE_DB_KEY);
if (is_array($course_list) && !empty($course_list)) {
    $course_list = $course_list[0];
}
$temp_course_list = $course_list;
// 利用しないコースを除外する
$course_list = [];
foreach($temp_course_list as $key=>$value){
    if(empty($value[COURSE_ACTIVATE]))continue;
    $course_list[$key] = $value;
}
global $parent_course_post_id;
$parent_slug = get_post($parent_course_post_id)->post_name; // 親ページのスラッグを取得
if(!empty($parent_slug)){
    $parent_slug = trim($parent_slug);
}
?><section id="p_main_v">
    <div class="sp">
        <img src="<?php echo get_template_directory_uri() ?>/image/<?php echo $parent_slug; ?>/sp/party/main.jpg">
        <p class="s_under"><img src="<?php echo get_template_directory_uri() ?>/image/<?php echo $parent_slug; ?>/sp/slide_under.jpg" alt="木目"></p>
    </div>
</section>
<section id="contents">
    <div id="about_main_cont" class="clearfix">
        <p id="pin"><img src="<?php echo get_template_directory_uri() ?>/image/<?php echo $parent_slug; ?>/common/pin.png" width="42" height="80"
                         alt="pin"></p>
        <!--LEFT_CONT-->
        <div id="left02">
            <section id="left_cont" class="clearfix center">
                <p class="a_cate"><img src="<?php echo get_template_directory_uri() ?>/image/<?php echo $parent_slug; ?>/common/pagemenu.png" width="180"
                                       height="20" alt="個室居酒屋 6年4組：パーティ ページメニュー"></p>
                <ul class="list2 fs04">
                    <p></p>
                    <?php
                    foreach ($course_list as $key => $value) {
                        ?>
                        <li><a href="#link<?php echo $key ?>">『<?php echo $value[COURSE_TITLE] ?>』</a></li>
                        <?php
                    }
                    ?>
                </ul>
            </section>
        </div>
        <p><!--MAIN_CONT--></p>
        <section id="main_r_cont">
            <p class="pankuzu fs02"><a href="./">TOP</a> &gt; パーティ</p>
            <p id="sub_title" class="fs09 bold">パーティ</p>
            <?php
            $free_data = get_post_meta($post->ID, COURSE_FREE_DATA , true);
            ?>
            <p><?php echo $free_data[COURSE_FREE_DATA_TITLE]?></p>
            <div>
                <?php echo str_replace(PHP_EOL , '<br/>' ,$free_data[COURSE_FREE_DATA_CONTENT])?>

            </div>
            <p><!--MAIN_CONT--></p>
            <ul id="party_thum" class="clearfix">
                <p></p>
                <?php
                foreach ($course_list as $key => $value) {
                    ?>
                    <!--【ここからコース内容】-->
                    <li><a href="#link<?php echo $key ?>"><img src="<?php echo $value[COURSE_IMAGE] ?>" width="180"
                                                               height="180"
                                                               alt="個室居酒屋 6年4組：『<?php echo $value[COURSE_TITLE] ?>』"></a><br>
                        <?php echo $value[COURSE_TITLE] ?></li>
                    <?php
                }
                ?>
            </ul>
            <?php
            foreach ($course_list as $key => $value) {
                ?>
                <ul class="fs07 bold party_ti clearfix" id="link<?php echo $key ?>">
                    <li class="floL"><?php echo $value[COURSE_TITLE] ?></li>
                    <li class="floL sub_title"><?php echo $value[COURSE_SUB_TITLE] ?></li>
                    <li class="floR textR fontSm"><?php echo $value[COURSE_PRICE] ?></li>
                </ul>
                <p class="center party_img"><img src="<?php echo $value[COURSE_IMAGE] ?>"
                                                 alt="個室居酒屋 6年4組：<?php echo $value[COURSE_TITLE] ?>"></p>
                <p class="info_txt fs04 center">
                    <span>品数：全 <span class="strong"><?php echo $value[COURSE_ARTICLES_COUNT] ?></span> 品　</span>｜<span>　人数： <span class="strong"><?php echo $value[COURSE_HUM_COUNT] ?> </span></span>｜<span><?php echo $value[COURSE_DELIVERY_TIME] ?></span>
                </p>
                <ul class="sub_txt">
                    <li><?php echo str_replace(PHP_EOL , '<br/>' ,$value[COURSE_DETAIL]) ?></li>
                </ul>
                <p class="pattern_top center">
                </p>
                <ul class="course_menu bold">
                    <li class="center fs06 bold">コースメニュー</li>
                    <?php echo str_replace(PHP_EOL , '<br/>' ,$value[COURSE_MENU]) ?>
                </ul>
                <p class="pattern_bottom center">
                </p>

                <div class="sub_txt course_detail">
                    <?php echo $value[COURSE_PLAN_DETAIL] ?>
                </div>
                <p><!--【ここまでコース内容】-->
                    <br>
                </p>
                <?php
            }
            ?><p>            <!--【ここまでコース内容】--><br>
                <!--ドリンク--></p>

            <?php
            $free_data = get_post_meta($post->ID, COURSE_FREE_UNDER_DATA , true);
            ?>

            <p class="fs07 bold party_ti"><?php echo $free_data[COURSE_FREE_UNDER_DATA_TITLE]?></p>
            <div>
                <?php echo str_replace(PHP_EOL , '<br/>' ,$free_data[COURSE_FREE_UNDER_DATA_CONTENT]) ?>

            </div>
        </section>
        <p id="go_top"><a href="#"><img src="<?php echo get_template_directory_uri() ?>/image/<?php echo $parent_slug; ?>/common/go_top_off.png"
                                        width="99" height="218" alt="トップへ"></a></p>
        <p></p></div>
</section>
