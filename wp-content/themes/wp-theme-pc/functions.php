<?php

//アイキャッチ画像
add_theme_support('post-thumbnails');
set_post_thumbnail_size(800, 9999);

include_once(get_template_directory() . '/include/custom_product_form.php');

//
//不要なタグを削除
//remove_action('wp_head', 'feed_links');
//remove_action('wp_head', 'feed_links_extra');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'wp_generator');




add_action('admin_print_scripts', 'my_admin_scripts');
add_action('admin_print_styles', 'my_admin_styles');
function my_admin_scripts()
{
    global $post;
    if (COURSE_CATEGORY_NAME == $post->post_type) {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_register_script('my-upload', get_bloginfo('template_directory') . '/js/admin.js');
        wp_enqueue_script('my-upload');
    }
}

function my_admin_styles()
{
    global $post;
    if (COURSE_CATEGORY_NAME == $post->post_type) {
        wp_enqueue_style('thickbox');
        wp_enqueue_style('admin_style',get_template_directory_uri()."/css/admin_style.css");
    }
}
function postlist($query) {
	if ( is_admin() || ! $query->is_main_query() )
		return;

	/*
	if ( $query->is_home() ) {
    $query->set( 'posts_per_page', '5' );
	}


	if ( $query->is_month() ) {
    $query->set( 'posts_per_page', '1' );
	}

	if ( $query->is_archive() ) {
    $query->set( 'posts_per_page', '1' );
	}
	*/

}
add_action( 'pre_get_posts', 'postlist' );

function isFirst(){
    global $wp_query;
    return ($wp_query->current_post === 0);
}

function isLast(){
    global $wp_query;
    return ($wp_query->current_post+1 === $wp_query->post_count);
}


// エディタでテンプレートパスを利用する ショートコード
function shortcode_templateurl() {
     return get_bloginfo('template_url');
}
add_shortcode('template_url', 'shortcode_templateurl');

// エディタでサイトパスを利用する ショートコード
function shortcode_siteurl() {
    return get_bloginfo('url');
}
add_shortcode('site_url', 'shortcode_siteurl');


// エディタでコースを表示させるためのattr
function course_loader($attr) {
    //
    $param = shortcode_atts(array('id'=>0) , $attr);

    $args = array(
        'post_type' => COURSE_CATEGORY_NAME
        ,'p'=>$param['id']
    );
    // 親取得
    global $post;
    $parent_id = $post->post_parent; // 親ページのIDを取得
    $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
    global $parent_course_post_id;
    $parent_course_post_id = $parent_id;
    // The Query
    $the_query = new WP_Query( $args );
    // The Loop
    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            get_template_part("/include/course_html_exporter");
        }
        wp_reset_postdata();
    }
}
add_shortcode('courseloader', 'course_loader');



// 追加CSS読み込み用
add_action('wp_enqueue_scripts', 'customCSSDataLoader');
function customCSSDataLoader()
{
    wp_enqueue_style('school_origin_style', get_template_directory_uri() . '/css/style.css#' . time(), array(), 0);
}

