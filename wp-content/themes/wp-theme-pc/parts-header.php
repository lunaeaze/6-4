  <header>
    <p id="main_logo"><a href="<?php echo home_url('/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/logo.png" width="194" height="241" alt="個室居酒屋 6年4組"></a></p>
    <p id="contact_btn"><a href="<?php echo home_url('/contact/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/contact-baloon.png" width="171" height="96" alt="お問い合わせ"></a></p>
    <nav class="clearfix col1 fs06">
      <ul id="left_menu">
        <li><a href="<?php echo home_url('/about/'); ?>">6年4組って</a></li>
        <li><a href="<?php echo home_url('/news/'); ?>">お知らせ</a></li>
        <li><a href="<?php echo home_url('/menu/'); ?>">メニュー</a></li>
      </ul>
      <ul id="right_menu">
        <li><a href="<?php echo home_url('/party/'); ?>">パーティ</a></li>
        <li><a href="<?php echo home_url('/photo/'); ?>">アルバム</a></li>
        <li><a href="<?php echo home_url('/shop/'); ?>">分校一覧</a></li>
        <li><a href="<?php echo home_url('/info/384'); ?>">先生募集</a></li>
      </ul>
    </nav>
  </header>