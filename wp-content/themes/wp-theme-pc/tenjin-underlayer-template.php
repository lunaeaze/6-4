<?php

/**
 * Template Name: 天神 下層 テンプレート
 * Created by PhpStorm.
 * User: Serinappa
 * Date: 2018/11/24
 * Time: 21:00
 *
 */
get_header('school'); ?>
    <!--
    =================
    TOP-CONTENTS
    =================-->
    <div class="tenjin">
        <div id="HeadTxt" class="pc">
            <h1 class="fs03 col1">福岡 天神でお誕生日会・女子会には学校シチュエーション個室居酒屋  6年4組</h1>
        </div>
        <!--HEADER-->
        <div class="pc">
        <section id="top_cont">
            <header>
                <div id="head_top" class="clearfix">
                    <p class="main_logo floL"><a href="<?php echo get_home_url()?>/tenjin"><img
                                    src="<?php echo get_template_directory_uri() ?>/image/common/logo.png" width="195"
                                    height="239"
                                    alt="個室居酒屋 6年4組"></a></p>
                    <p class="shop_name floL bold">学校居酒屋 6年4組<br>
                        <span class="s_name" id="name_tenjin">天神分校</span></p>
                    <p class="tel floL fs10 bold">092-781-0604</p>
                </div>
                <p id="contact_btn"><a href="http://www.6nen4kumi.com/contact/" target="_blank"><img
                                src="<?php echo get_template_directory_uri() ?>/image/common/contact.png" width="238" height="110"
                                alt="学校居酒屋 6年4組 天神分校:お問い合わせ"></a></p>
                <nav class="clear">
                    <ul class="navi floR col1 fs07">
                        <!--<li><a href="./">ホーム</a></li>-->
                        <li><a href="<?php echo get_home_url()?>/tenjin/about">6年4組って？</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/menu">アラカルト</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/party">コース</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/coupon">クーポン</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/access">アクセス</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/room">教室一覧</a></li>
                    </ul>
                </nav>
            </header>
        </section>
    </div>
        <div class="sp">
            <header>
                <div><a href="<?php echo get_home_url()?>/tenjin"><img src="<?php echo get_template_directory_uri() ?>/image/tenjin/sp/haeder.jpg" alt="福岡 天神でお誕生日会・女子会には学校シチュエーション個室居酒屋  6年4組"></a></div>
                <p id="panel-btn"><img  src="<?php echo get_template_directory_uri() ?>/image/tenjin/sp/pull_menu.png" alt="メニュー"></p>
                <nav id="panel">
                    <ul>
                        <li><a href="<?php echo get_home_url()?>/tenjin/access">アクセス</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/about">6年4組の魅力</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/menu">アラカルト</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/party">コース</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/coupon">クーポン</a></li>
                        <li><a href="<?php echo get_home_url()?>/tenjin/room">教室（個室）一覧</a></li>
                        <li><a href="http://www.6nen4kumi.com/shop/">分校（お店）一覧</a></li>
                        <li><a href="tel:0927810604" onclick="ga('send', 'event', 'smartphone', 'phone-number-tap', 'tenjin');">電話予約</a></li>
                        <li><a href="https://www.hotpepper.jp/strJ001153861/yoyaku/hpds/">WEB予約</a></li>
                        <li><a href="http://www.6nen4kumi.com/">6年4組総合TOP</a></li>
                    </ul>
                    <div id="page_x"><img  src="<?php echo get_template_directory_uri() ?>/image/tenjin/sp/pull_xx_btn.jpg" alt="閉じる"></div>
                </nav>
            </header>
        </div>

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php the_content(); ?>

        <?php endwhile; endif; ?>

    </div>
<?php get_footer('school'); ?>