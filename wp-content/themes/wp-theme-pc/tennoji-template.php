<?php

/**
 * Template Name: 天王寺 テンプレート
 * Created by PhpStorm.
 * User: Serinappa
 * Date: 2018/11/24
 * Time: 21:00
 *
 */
get_header('school'); ?>
    <!--
    =================
    TOP-CONTENTS
    =================-->
    <div class="tennoji home">
        <div id="HeadTxt" class="pc">
            <h1 class="fs03 col1">阿倍野天王寺でお誕生日会・女子会には学校シチュエーション個室居酒屋 6年4組</h1>
        </div>
        <!--HEADER-->
        <div class="pc">
            <section id="top_cont">
                <header>
                    <div id="head_top" class="clearfix">
                        <p class="main_logo floL"><a href="<?php echo get_home_url()?>/tennoji"><img
                                        src="<?php echo get_template_directory_uri() ?>/image/common/logo.png"
                                        width="195"
                                        height="239"
                                        alt="個室居酒屋 6年4組"></a></p>
                        <p class="shop_name floL bold">学校居酒屋 6年4組<br>
                            <span class="s_name" id="name_abeno">阿倍野天王寺駅前分校</span></p>
                        <p class="tel floL fs10 bold">06-6625-0604</p>
                    </div>
                    <p id="contact_btn"><a href="http://www.6nen4kumi.com/contact/" target="_blank"><img
                                    src="<?php echo get_template_directory_uri() ?>/image/common/contact.png"
                                    width="238"
                                    height="110"
                                    alt="学校居酒屋 6年4組 阿倍野天王寺駅前分校:お問い合わせ"></a></p>
                    <nav class="clear">
                        <ul class="navi floR col1 fs07">
                            <!--<li><a href="./">ホーム</a></li>-->
                            <li><a href="<?php echo get_home_url()?>/tennoji/about">6年4組って？</a></li>
                            <li><a href="<?php echo get_home_url()?>/tennoji/menu">アラカルト</a></li>
                            <li><a href="<?php echo get_home_url()?>/tennoji/party">コース</a></li>
                            <li><a href="<?php echo get_home_url()?>/tennoji/coupon">クーポン</a></li>
                            <li><a href="<?php echo get_home_url()?>/tennoji/access">アクセス</a></li>
                            <li><a href="<?php echo get_home_url()?>/tennoji/room">教室一覧</a></li>
                        </ul>
                    </nav>
                </header>
            </section>
        </div>


        <div class="sp">
            <header>
                <div><a href="<?php echo get_home_url()?>/tennoji"><img src="<?php echo get_template_directory_uri() ?>/image/tennoji/sp/haeder.jpg"
                                           alt="阿倍野天王寺でお誕生日会・女子会には学校シチュエーション個室居酒屋  6年4組"></a></div>
                <p id="panel-btn"><img src="<?php echo get_template_directory_uri() ?>/image/tennoji/sp/pull_menu.png"
                                       alt="メニュー"></p>
                <nav id="panel">
                    <ul>
                        <li><a href="<?php echo get_home_url()?>/tennoji/access">アクセス</a></li>
                        <li><a href="<?php echo get_home_url()?>/tennoji/about">6年4組の魅力</a></li>
                        <li><a href="<?php echo get_home_url()?>/tennoji/menu">アラカルト</a></li>
                        <li><a href="<?php echo get_home_url()?>/tennoji/party">コース</a></li>
                        <li><a href="<?php echo get_home_url()?>/tennoji/coupon">クーポン</a></li>
                        <li><a href="<?php echo get_home_url()?>/tennoji/room">教室（個室）一覧</a></li>
                        <li><a href="http://www.6nen4kumi.com/shop/">分校（お店）一覧</a></li>
                        <li><a href="tel:0666250604" onclick="ga('send', 'event', 'smartphone', 'phone-number-tap', 'tennoji');">電話予約</a></li>
                        <li><a href="https://www.hotpepper.jp/strJ001153861/yoyaku/hpds/">WEB予約</a></li>
                        <li><a href="http://www.6nen4kumi.com/">6年4組総合TOP</a></li>
                    </ul>
                    <div id="page_x"><img
                                src="<?php echo get_template_directory_uri() ?>/image/tennoji/sp/pull_xx_btn.jpg"
                                alt="閉じる"></div>
                </nav>
            </header>
        </div>
        <section id="main_v">
            <div class="sp">

                <!--SLIDER-->
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <img class="swiper-slide" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide1.jpg" />
                        <img class="swiper-slide" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide2.jpg" />
                        <img class="swiper-slide" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide3.jpg" />
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination swiper-pagination-white"></div>
                </div>
            </div>
            <p class="main_txt col1">天王寺駅徒歩1分。総席数 70席 ★17:00～23:30（ラストオーダー　23:00）<br>
                入口の黒板や懐かしの机など小学校のシチュエーションを完全再現した6年4組 阿倍野天王寺駅前分校。
                各コンセプトに沿ってつくられた部屋は、大きな黒板やランドセルなど学校の雰囲気そのもの！
                青空と鉄棒にバスケットゴールなど、まるでグラウンドを演出したお部屋も新登場☆
                人数に合わせて教室を用意してくれるので、飲み会はもちろん女子会からお誕生日会まで、どんなシーンにもOK！<br>
                アラカルトの場合は、おひとりさま1ドリンク・2オーダー以上のご注文になります。<br>
                10名様以上のご予約の場合はコース料理のご注文となります。</p>
        </section>
        <!--CONTENTS-->
        <section id="contents">
            <div id="main_cont" class="clearfix">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <?php the_content(); ?>

                <?php endwhile; endif; ?>
            </div>

            <?php
            get_template_part('/view/common/link_list');
            ?>
            <?php
            get_template_part('/view/footer/link_btns');
            ?>

        </section>


    </div>


<?php get_footer('school'); ?>