<?php
get_header();
$post_cat = get_the_category();
$post_cat_name = $post_cat[ 0 ]->name;
$post_cat_id = $post_cat[ 0 ]->cat_ID;
foreach ( $post_cat as $v ) {
	if ( $v->parent != 0 ) {
		$post_cat_name = $v->name;
		$post_cat_id = $v->cat_ID;
	}
}
$color = get_field( 'color', 'category_' . $post_cat_id );
?>

<!--====== === === ===
 ==TOP - CONTENTS ===
== === === === == -->
	<!--HEADER-->

	<section id="top_cont">
		<?php get_template_part('parts', 'header'); ?>

		<!--SLIDER-->
        <div class="swiper-container pc">
            <div class="swiper-wrapper">
                <div class="swiper-slide" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide1.jpg)"></div>
                <div class="swiper-slide" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide2.jpg)"></div>
                <div class="swiper-slide" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide3.jpg)"></div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-white"></div>
        </div>
        <div class="swiper-container-sp sp">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide1.jpg"> </div>
                <div class="swiper-slide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide2.jpg"> </div>
                <div class="swiper-slide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide3.jpg"> </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination-sp swiper-pagination-white"></div>
        </div>
		<!--LINK-->
		<ul class="link_btn clearfix">
			<!--<li class="center"><a href="http://www.6nen4kumi.com/bounenkai/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/link01_bounen_off.png" width="293" height="180" alt="個室家座香屋 6年4組:★忘年会コース★"></a><br>★忘年会★</li>-->
			<li class="center"><a href="<?php echo home_url('/about/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/link01_off.png" alt="個室家座香屋 6年4組:校舎について"></a><br> ★6年4組って★
			</li>
			<li class="center"><a href="<?php echo home_url('/menu/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/link02_off.png"  alt="個室家座香屋 6年4組:食べ放題メニュー"></a><br> ★メニュー★
			</li>
			<li class="center"><a href="<?php echo home_url('/party/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/link03_off.png" alt="個室家座香屋 6年4組:女子会-歓迎会"></a><br> ★パーティ★
			</li>
		</ul>
	</section>

<!--CONTENTS-->
<section id="contents">
	<div id="main_cont" class="clearfix">
		<p id="pin"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/pin.png" width="42" height="80" alt="pin">
		</p>

		<!--MAIN_CONT-->
		<section id="main_r_cont">
			<p class="sub_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/news_title.png" width="600" height="21" alt="お知らせ">
			</p>
			<ul id="news">
				<?php query_posts('cat=1&posts_par_page=5'); if(have_posts()): while(have_posts()): the_post(); ?>
				<li>
					<?php the_time('Y.m.d'); ?>
					<span> 新着お知らせ </span>
					<a href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</li>
				<?php endwhile; endif; ?>
			</ul>
			<p class="sub_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/menu_title.png" width="600" height="21" alt="新メニュー紹介">
			</p>
			<ul id="news">
				<?php query_posts('cat=7&posts_par_page=5'); if(have_posts()): while(have_posts()): the_post(); ?>
				<li>
					<?php the_time('Y.m.d'); ?>
					<span>
						<?php $cat = get_the_category(); ?>
						<?php $cat = $cat[0]; ?>
						<?php echo get_cat_name($cat->term_id); ?>
					</span>
					<a href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</li>
				<?php endwhile; endif; ?>
			</ul>
			<p class="sub_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/media_title.png" width="600" height="21" alt="お知らせ">
			</p>
			<ul id="media">
				<?php query_posts('cat=2&posts_par_page=5'); if(have_posts()): while(have_posts()): the_post(); ?>
				<li>
					<?php the_time('Y.m.d'); ?>
					<span>メディア掲載</span>
					<a href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</li>
				<?php endwhile; endif; ?>
			</ul>
			<p class="sub_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop_title.png" width="600" height="25" alt="教室の場所">
			</p>
			<ul id="shop">
				<li>
					<a href="<?php echo home_url('/umeda/'); ?>"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop01.jpg" width="174" height="174" alt="個室家座香屋 6年4組:梅田分校"></a><br><span class="map"> <a href="https://goo.gl/maps/FRrDf" target="_blank">地図</a> </span>
				</li>

				<li>
					<a href="<?php echo home_url('/tennoji/'); ?>"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop03.jpg" width="174" height="174" alt="個室家座香屋 6年4組:阿倍野天王寺駅前分校"></a><br><span class="map"> <a href="https://goo.gl/maps/zWVwg" target="_blank">地図</a></span>
				</li>
				<li>
					<a href="<?php echo home_url('/tenjin/'); ?>"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop04.jpg" width="174" height="174" alt="個室家座香屋 6年4組:天神分校"></a><br><span class="map"> <a href="https://goo.gl/maps/07Xf1" target="_blank">地図</a></span>
				</li>

				<li class="y-outer_r"> <span class="yoyaku r"> <a href="https://www.hotpepper.jp/strJ000717096/yoyaku/hpds/" target="_blank" title="6年4組:梅田分校:WEB予約">WEB予約</a></span>
				</li>
				<li class="y-outer_b"> <span class="yoyaku b"> <a href="https://www.hotpepper.jp/strJ001099992/yoyaku/hpds/" target="_blank" title="6年4組:阿倍野天王寺駅前分校:WEB予約">WEB予約</a></span>
				</li>
				<li class="y-outer_g"> <span class="yoyaku g"> <a href="https://www.hotpepper.jp/strJ000762080/yoyaku/hpds/" target="_blank" title="6年4組:天神分校:WEB予約">WEB予約</a></span>
				</li>

				<li>
					<a href="http://www.6nen4kumi.com/shibuya/"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop05.jpg" width="174" height="174" alt="個室家座香屋 6年4組:渋谷第一分校"></a><br><span class="map"> <a href="https://goo.gl/maps/qgcvP" target="_blank">地図</a></span>
				</li>

				<li>
					<a href="<?php echo home_url('/shinjuku/'); ?>" target="_blank"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop06.jpg" width="174" height="174" alt="個室家座香屋 6年4組:新宿東口駅前分校"></a><br><span class="map"> <a href="https://goo.gl/maps/fuIFD" target="_blank">地図</a></span>
				</li>

				<li>
					<a href="<?php echo home_url('/ikebukuro/'); ?>"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop08.jpg" width="174" height="174" alt="個室家座香屋 6年4組:池袋分校"></a><br><span class="map"> <a href="https://goo.gl/maps/7jmW9aJziik" target="_blank">地図</a></span>
				</li>

				<li class="y-outer_b"> <span class="yoyaku b"> <a href="https://www.hotpepper.jp/strJ000768666/yoyaku/hpds/" target="_blank" title="6年4組:渋谷第一:WEB予約">WEB予約</a></span>
				</li>
				<li class="y-outer_g"> <span class="yoyaku g"> <a href="https://www.hotpepper.jp/strJ001108736/yoyaku/hpds/" target="_blank" title="6年4組:新宿東口前分校:WEB予約">WEB予約</a></span>
				</li>
				<li class="y-outer_r"> <span class="yoyaku r"> <a href="https://www.hotpepper.jp/strJ001153861/yoyaku/hpds/" target="_blank" title="6年4組:池袋東口分校:WEB予約">WEB予約</a></span>
				</li>

				<li>
					<a href="<?php echo home_url('/nagoya/'); ?>"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop-nagoya.jpg" width="174" height="174" alt="個室家座香屋 6年4組:名古屋名駅分校"></a><br><span class="map"> <a href="https://goo.gl/maps/tfTcnJRYCqy" target="_blank">地図</a></span>
				</li>

				<li>
					<a href="<?php echo home_url('/sakae/'); ?>"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop-sakae.jpg" width="174" height="174" alt="個室家座香屋 6年4組:栄分教場"></a><br><span class="map"> <a href="https://goo.gl/maps/avmBrFxwNoQ2" target="_blank">地図</a></span>
				</li>

			</ul>
			<ul class="shop">
				<li class="y-outer_b"> <span class="yoyaku b"> <a href="https://www.hotpepper.jp/strJ001192959/yoyaku/hpds/" target="_blank" title="6年4組:名古屋名駅分校:WEB予約">WEB予約</a></span>
				</li>
				<li class="y-outer_g"> <span class="yoyaku g"> <a href="https://www.hotpepper.jp/strJ001202008/yoyaku/hpds" target="_blank" title="6年4組:栄分教場:WEB予約">WEB予約</a></span>
				</li>
			</ul>
			<!--p class="center"><a href="http://r.gnavi.co.jp/seg3v6m50000/" target="_blank"><img style="border:5px solid #fff;" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/343.jpg" width="590" height="auto" alt="元祖343鮨 銀座本店"></a></p-->
			<!--p class="floL"><a href="<?php echo home_url('/contact/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/tv.jpg" width="281" height="150" alt="TV取材はこちら"></a></p>
      <p class="floR"><a href="<?php echo home_url('/contact/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/cora.jpg" width="281" height="150" alt="コラボタイアップお申し込み"></a></p-->
		</section>
        <!--LEFT_CONT-->
        <section id="left_cont" class="clearfix">
            <li><iframe width="100%" height="210" src="https://www.youtube.com/embed/fJG68ypGEeg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </li>
            <p class="sns_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/sub_title01.png" width="97" height="24" alt="公式SNS">
            </p>

            <!--<div id="insta_area">
     <p class="insta_logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/insta_logo.png" width="69" height="19" alt="公式instagram"></p>
      <div class="instagram center"></div>
      </div> -->

            <div id="insta_area">
                <p class="insta_logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/insta_logo.png" width="69" height="19" alt="公式instagram">
                </p>
                <?php echo do_shortcode('[instagram-feed]'); ?>
            </div>
            <a class="twitter-timeline" href="https://twitter.com/hashtag/6%E5%B9%B44%E7%B5%84" data-widget-id="644799901818220544">#6年4組 のツイート</a>
            <script>
                ! function ( d, s, id ) {
                    var js, fjs = d.getElementsByTagName( s )[ 0 ],
                        p = /^http:/.test( d.location ) ? 'http' : 'https';
                    if ( !d.getElementById( id ) ) {
                        js = d.createElement( s );
                        js.id = id;
                        js.src = p + "://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore( js, fjs );
                    }
                }( document, "script", "twitter-wjs" );
            </script>

            <!--<a class="twitter-timeline" href="https://twitter.com/64group" data-widget-id="644800135029911556">@64groupさんのツイート</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>-->

            <!--<ul class="center banner">
        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/banner/banner01_off.png" width="315" height="109" alt="学級だより"></li>
        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/banner/banner02_off.png" width="315" height="109" alt="夏のあげパン祭り"></li>
        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/banner/banner03_off.png" width="315" height="109" alt="先生募集"></li>
      </ul>-->
            <ul class="a_list">
                <li><a href="http://www.6nen4kumi.com/info/646/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/dagashi.png" width="281" height="150" alt="駄菓子"></a> </li>
                <li><a href="<?php echo home_url('/contact/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/tv.jpg" width="281" height="150" alt="TV取材はこちら"></a> </li>
                <!--/ul>
      <ul class="a_list"-->
                <li><a href="<?php echo home_url('/contact/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/cora.jpg" width="281" height="150" alt="コラボタイアップお申し込み"></a> </li>
                <li><a href="http://www.6nen4kumi.com/info/384/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/rec.jpg" width="281" height="150" alt="先生募集"></a> </li>
            </ul>
        </section>
	</div>
	<div class="contwrap clearfix">
		<h2>6年4組は小学校をコンセプトとしています。</h2>
		<p>あの頃の懐かしい気持ちになる、小学校のシチュエーションを完全再現した店内や、<br>揚げパンやソフト麺など懐かしの給食メニューの数々。<br> お店のスタッフは「学校の先生」お客様は「生徒」です。スタッフ全員、先生らしく接客いたします。
			<br> 名物は、参加者全員が受ける「抜き打ちテスト」です。勉強も忘れずに♪ぜひご来校ください。
		</p>
	</div>
	<p id="r18"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/r18.png" alt="未成年飲酒防止の為、年齢確認の強化・誓約書の記入をお願いしております。">
	</p>
	<div class="vrwrap clearfix">
		<h2>VRで見える!6年4組</h2>
		<p><a herf="<?php echo get_stylesheet_directory_uri(); ?>/image/index/vr-64.jpg" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/vr-64.jpg" width="100%" height="auto" alt="VRで見える!6年4組"></a></p>
		<p>今すぐ小学校のあのころ体験！画像クリックしてVRゴーグルで覗いてみよう!</p>
	</div>
	<p id="greeting" class="clear"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/greeting.png" width="992" height="684" alt="asdf"></p>
		<div class="contwrap clearfix" id="notice-copy">
		<h2>ご注意</h2>
		<p>最近「家座香屋6年4組」に似たお店が数多くあります。阪神食品株式会社の運営する学校居酒屋は「家座香屋6年4組」のみです。誤解されたお客様には大変申し訳ございません。「家座香屋6年4組」には商標登録・実用新案の飲食店です。本物をご利用ください！</p>
	</div>
	</div>
</section>
<script type="text/javascript">
	window._pt_lt = new Date().getTime();
	window._pt_sp_2 = [];
	_pt_sp_2.push( 'setAccount,6da25f48' );
	var _protocol = ( ( "https:" == document.location.protocol ) ? " https://" : " http://" );
	( function () {
		var atag = document.createElement( 'script' );
		atag.type = 'text/javascript';
		atag.async = true;
		atag.src = _protocol + 'js.ptengine.jp/pta.js';
		var stag = document.createElement( 'script' );
		stag.type = 'text/javascript';
		stag.async = true;
		stag.src = _protocol + 'js.ptengine.jp/pts.js';
		var s = document.getElementsByTagName( 'script' )[ 0 ];
		s.parentNode.insertBefore( atag, s );
		s.parentNode.insertBefore( stag, s );
	} )();
</script> 
<?php get_footer(); ?>