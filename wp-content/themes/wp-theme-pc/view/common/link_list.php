<?php
/**
 * Created by PhpStorm.
 * User: Serinappa
 * Date: 2018/11/24
 * Time: 20:51
 */
?>

<!--SNS_SHOP-->
<section id="mid_cont" class="clearfix">
    <!--<div id="twitter" class="floL"><a class="twitter-timeline" href="https://twitter.com/hashtag/6%E5%B9%B44%E7%B5%84" data-widget-id="644799901818220544">#6年4組 のツイート</a>
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </div>-->
    <div id="shop_area" class="floR">
        <ul id="shop">
            <li><a href="http://www.6nen4kumi.com/umeda/"><img
                            src="<?php echo get_template_directory_uri() ?>/image/index/shop/shop01.jpg" width="174"
                            height="174" alt="個室居酒屋 6年4組:梅田分校"></a><br>
                <span class="map"><a href="https://goo.gl/maps/FRrDf" target="_blank">地図<img
                                src="<?php echo get_template_directory_uri() ?>/image/index/map_pin.png" width="15"
                                height="24" alt="大きな地図へ" class="map_pin"></a></span></li>
            <li><a href="http://www.6nen4kumi.com/tennoji/"><img
                            src="<?php echo get_template_directory_uri() ?>/image/index/shop/shop03.jpg" width="174"
                            height="174" alt="個室居酒屋 6年4組:阿倍野天王寺駅前分校"></a><br>
                <span class="map"><a href="https://goo.gl/maps/zWVwg" target="_blank">地図<img
                                src="<?php echo get_template_directory_uri() ?>/image/index/map_pin.png" width="15"
                                height="24" alt="大きな地図へ" class="map_pin"></a></span></li>
            <li><a href="http://www.6nen4kumi.com/tenjin/"><img
                            src="<?php echo get_template_directory_uri() ?>/image/index/shop/shop04.jpg" width="174"
                            height="174" alt="個室居酒屋 6年4組:天神分校"></a><br>
                <span class="map"><a href="https://goo.gl/maps/07Xf1" target="_blank">地図<img
                                src="<?php echo get_template_directory_uri() ?>/image/index/map_pin.png" width="15"
                                height="24" alt="大きな地図へ" class="map_pin"></a></span></li>
            <li><a href="http://www.6nen4kumi.com/shibuya/"><img
                            src="<?php echo get_template_directory_uri() ?>/image/index/shop/shop05.jpg" width="174"
                            height="174" alt="個室居酒屋 6年4組:渋谷第一分校"></a><br>
                <span class="map"><a href="https://goo.gl/maps/qgcvP" target="_blank">地図<img
                                src="<?php echo get_template_directory_uri() ?>/image/index/map_pin.png" width="15"
                                height="24" alt="大きな地図へ" class="map_pin"></a></span></li>
            <!--<li><a href="http://www.6nen4kumi.com/shibuya-u19/"><img src="<?php echo get_template_directory_uri() ?>/image/index/shop/shop05_2.jpg" width="174" height="174" alt="個室居酒屋 6年4組:カフェ U19渋谷第二分校"></a><br>
            <span class="map"><a href="https://goo.gl/maps/qgcvP" target="_blank">地図<img src="<?php echo get_template_directory_uri() ?>/image/index/map_pin.png" width="15" height="24" alt="大きな地図へ" class="map_pin"></a></span></li>-->
            <li><a href="http://www.6nen4kumi.com/shinjuku/"><img
                            src="<?php echo get_template_directory_uri() ?>/image/index/shop/shop06.jpg" width="174"
                            height="174" alt="個室居酒屋 6年4組:新宿東口駅前分校"></a><br>
                <span class="map"><a href="https://goo.gl/maps/x6UXZDtWmNB2" target="_blank">地図<img
                                src="<?php echo get_template_directory_uri() ?>/image/index/map_pin.png" width="15"
                                height="24" alt="大きな地図へ" class="map_pin"></a></span></li>
            <li><a href="http://www.6nen4kumi.com/ikebukuro/"><img
                            src="<?php echo get_template_directory_uri() ?>/image/index/shop/shop08.jpg" width="174"
                            height="174" alt="個室居酒屋 6年4組:池袋分校"></a><br>
                <span class="map"><a href="https://goo.gl/maps/iZFNHxxe5E92" target="_blank">地図<img
                                src="<?php echo get_template_directory_uri() ?>/image/index/map_pin.png" width="15"
                                height="24" alt="大きな地図へ" class="map_pin"></a></span></li>
            <li><a href="http://www.6nen4kumi.com/nagoya/"><img
                            src="<?php echo get_template_directory_uri() ?>/image/index/shop/shop-nagoya.jpg"
                            width="174" height="174" alt="個室居酒屋 6年4組:名古屋名駅分校"></a><br>
                <span class="map"><a href="https://goo.gl/maps/wRup1WjWW1K2" target="_blank">地図<img
                                src="<?php echo get_template_directory_uri() ?>/image/index/map_pin.png" width="15"
                                height="24" alt="大きな地図へ" class="map_pin"></a></span></li>
            <li><a href="http://www.6nen4kumi.com/sakae"><img
                            src="<?php echo get_template_directory_uri() ?>/image/index/shop/shop-sakae.jpg" width="174"
                            height="174" alt="個室居酒屋 6年4組:栄分教場"></a><br>
                <span class="map"><a href="https://goo.gl/maps/avmBrFxwNoQ2" target="_blank">地図<img
                                src="<?php echo get_template_directory_uri() ?>/image/index/map_pin.png" width="15"
                                height="24" alt="大きな地図へ" class="map_pin"></a></span></li>
        </ul>
    </div>
</section>
