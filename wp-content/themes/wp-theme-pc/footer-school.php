<p id="go_top_" class="sp"><a href="#"><img src="<?php echo get_template_directory_uri() ?>/image/totop.png" alt="トップへ"></a></p>
<!--FOOTER-->
<footer>
  <ul class="col1 fs02 center">
    <li>個室居酒屋6年4組</li>
    <li>|</li>
    <li><a href="<?php echo home_url('/'); ?>">ホーム</a></li>
    <li>|</li>
    <li><a href="<?php echo home_url('/menu/'); ?>">メニュー</a></li>
    <li>|</li>
    <li><a href="http://www.hanshin-shokuhin.co.jp/index.html" target="_blank">運営会社</a></li>
    <li>|</li>
    <li><a href="<?php echo home_url('/shop/'); ?>">店舗情報</a></li>
    <li>|</li>
    <li><a href="<?php echo home_url('/contact/'); ?>">お問合せ</a></li>
    <li>|</li>
    <li>Copyright &copy; 2015　阪神食品株式会社　All Rights Reserved</li>
  </ul>
</footer>
<!--
=================================
JS
=================================--> 
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/smartRollover.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.instagram.js"></script>
<script>
    $(document).ready(function() {
     var clientId = '4a6ea578b6924136828b6839bdbce5c3';


      $(".instagram").instagram({
    	accessToken: "1245217885.c89113e.5ba233eec36446b782942279b7ca1f6b",
  	userId: "2160262579",
        show: 10,
        clientId: clientId
      });


    });
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/swiper.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
		speed: 2000,
		autoplay: 3000,
        spaceBetween: 30,
        effect: 'fade'
    });
</script>
<script> 
$(function(){ 
   // #で始まるアンカーをクリックした場合に処理 
   $('a[href^=#]').click(function() { 
      // スクロールの速度 
      var speed = 600; // ミリ秒 
      // アンカーの値取得 
      var href= $(this).attr("href"); 
      // 移動先を取得 
      var target = $(href == "#" || href == "" ? 'html' : href); 
      // 移動先を数値で取得 
      var position = target.offset().top; 
      // スムーススクロール 
      $('body,html').animate({scrollTop:position}, speed, 'swing'); 
      return false; 
   }); 
}); 
</script>
<script>
$(function() {
	var topBtn = $('#go_top,#go_top_');
	//スクロールしてトップ
    topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
    });
});

</script>
<script src="<?php echo get_template_directory_uri() ?>/js/sp/pull.js"></script>
<?php wp_footer(); ?>
</body>
</html>