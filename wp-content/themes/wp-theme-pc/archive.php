<?php get_header(); ?>
<!--
=================
TOP-CONTENTS
=================--> 
<!--HEADER-->
<section id="a_top_cont">

	<?php get_template_part('parts', 'header'); ?>

</section>

<!--CONTENTS-->
<section id="contents">
  <div id="main_cont" class="clearfix">
    <p id="pin"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/pin.png" width="42" height="80" alt="pin"></p>

<?php get_sidebar(); ?>
    
    <!--MAIN_CONT-->
    <section id="main_r_cont">
	    <p class="pankuzu fs02"><a href="<?php echo home_url('/'); ?>">TOP</a> > <a href="<?php echo home_url('/news/'); ?>">最新記事</a> > <?php wp_title(''); ?></p>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	    <div class="post">
		    <p class="post_day fs03"><?php the_time('Y年m月d日'); ?></p>
		    <p class="post_title fs07 bold"><?php the_title(); ?></p>
		    <?php the_content(); ?>
	    </div>

<?php endwhile; endif; ?>

			<?php if(function_exists('wp_pagenavi')){ wp_pagenavi(); } ?>

    </section>

  </div>
</section>
  
<?php get_footer(); ?>