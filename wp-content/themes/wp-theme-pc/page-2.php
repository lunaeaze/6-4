<?php get_header(); ?>

<!--
=================
TOP-CONTENTS
=================--> 
<!--HEADER-->

<section id="top_cont">

<?php get_template_part('parts', 'header'); ?>

  <!--SLIDER-->
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide1.jpg)"></div>
      <div class="swiper-slide" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide2.jpg)"></div>
      <div class="swiper-slide" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide3.jpg)"></div>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination swiper-pagination-white"></div>
  </div>
  <!--LINK-->
  <ul class="link_btn clearfix">
    <li class="center"><a href="<?php echo home_url('/about/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/link01_off.png" width="293" height="180" alt="個室家座香屋 6年4組:校舎について"></a><br>
      ★6年4組って★</li>
    <li class="center"><a href="<?php echo home_url('/menu/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/link02_off.png" width="293" height="180" alt="個室家座香屋 6年4組:食べ放題メニュー"></a><br>
      ★メニュー★</li>
    <li class="center"><a href="<?php echo home_url('/party/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/link03_off.png" width="293" height="180" alt="個室家座香屋 6年4組:女子会-歓迎会"></a><br>
      ★パーティ☆</li>
  </ul>
</section>

<!--CONTENTS-->
<section id="contents">
  <div id="main_cont" class="clearfix"> 
    <p id="pin"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/pin.png" width="42" height="80" alt="pin"></p>
    <!--LEFT_CONT-->
    <section id="left_cont" class="clearfix">
      <p class="sns_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/sub_title01.png" width="97" height="24" alt="公式SNS"></p>
      
      <!--<div id="insta_area">
     <p class="insta_logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/insta_logo.png" width="69" height="19" alt="公式instagram"></p>
      <div class="instagram center"></div>
      </div> -->

<div id="insta_area">
<p class="insta_logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/insta_logo.png" width="69" height="19" alt="公式instagram"></p>
<iframe src="http://snapwidget.com/sl/?h=NuW5tDTntYR8aW58MjgwfDN8M3x8bm98MjB8bm9uZXxvblN0YXJ0fHllc3xubw==&ve=051015" title="Instagram Widget" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:300px; height:300px"></iframe></div>

    
      <a class="twitter-timeline" href="https://twitter.com/hashtag/6%E5%B9%B44%E7%B5%84" data-widget-id="644799901818220544">#6年4組 のツイート</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

      <!--<a class="twitter-timeline" href="https://twitter.com/64group" data-widget-id="644800135029911556">@64groupさんのツイート</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>-->


      
      <!--<ul class="center banner">
        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/banner/banner01_off.png" width="315" height="109" alt="学級だより"></li>
        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/banner/banner02_off.png" width="315" height="109" alt="夏のあげパン祭り"></li>
        <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/banner/banner03_off.png" width="315" height="109" alt="先生募集"></li>
      </ul>-->

    </section>
    
    <!--MAIN_CONT-->
    <section id="main_r_cont">
      <p class="sub_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/news_title.png" width="600" height="21" alt="お知らせ"></p>
      <ul id="news">

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <li><?php the_time('Y.m.d'); ?>　<span><?php $cats = get_the_category(); $cat = $cats[0]; echo $cat->cat_name; ?></span>　<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endwhile; endif; ?>

      </ul>
<p class="sub_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop_title.png" width="600" height="25" alt="教室の場所"></p>
      
      <ul id="shop">
        <li><a href="<?php echo home_url('/umeda/'); ?>">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop01.jpg" width="174" height="174" alt="個室家座香屋 6年4組:梅田分校"></a><br>
          <span class="map">
          <a href="https://goo.gl/maps/FRrDf" target="_blank">地図</a>
          </span></li>
          
        <li><a href="<?php echo home_url('/kitashinti/'); ?>">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop02.jpg" width="174" height="174" alt="個室家座香屋 6年4組:北新地分校"></a><br>
          <span class="map">
          <a href="https://goo.gl/maps/mH98O" target="_blank">地図</a>
          </span></li>

        <li><a href="<?php echo home_url('/tennoji/'); ?>">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop03.jpg" width="174" height="174" alt="個室家座香屋 6年4組:阿倍野天王寺駅前分校"></a><br>
          <span class="map">
          <a href="https://goo.gl/maps/zWVwg" target="_blank">地図</a>
          </span></li>

<li><span class="yoyaku"><a href="http://r.gnavi.co.jp/plan/k495108/plan-reserve/plan/plan_list/?coursetime=1900&minmember=&maxmember=&action_id=Plan_list&date=20160210" target="_blank" title="6年4組:梅田分校:WEB予約">WEB予約</a></span></li>
<li><span class="yoyaku"><a href="http://r.gnavi.co.jp/plan/k495111/plan-reserve/plan/plan_list/" target="_blank" title="6年4組:北新地分校:WEB予約">WEB予約</a></span></li>
<li><span class="yoyaku"><a href="http://r.gnavi.co.jp/plan/5h1zhyvn0000/plan-reserve/plan/plan_list/" target="_blank" title="6年4組:阿倍野天王寺駅前分校:WEB予約">WEB予約</a></span></li>

        <li><a href="<?php echo home_url('/tenjin/'); ?>">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop04.jpg" width="174" height="174" alt="個室家座香屋 6年4組:天神分校"></a><br>
          <span class="map">
          <a href="https://goo.gl/maps/07Xf1" target="_blank">地図</a>
          </span></li>

        <li><a href="http://www.6nen4kumi.com/shibuya/">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop05.jpg" width="174" height="174" alt="個室家座香屋 6年4組:渋谷第一、第二分校"></a><br>
          <span class="map">
          <a href="https://goo.gl/maps/qgcvP" target="_blank">地図</a>
          </span></li>

        <li><a href="http://www.6nen4kumi.com/shinjuku/" target="_blank">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop06.jpg" width="174" height="174" alt="個室家座香屋 6年4組:新宿東口駅前分校"></a><br>
          <span class="map">
          <a href="https://goo.gl/maps/fuIFD" target="_blank">地図</a>
          </span></li>

<li><span class="yoyaku"><a href="http://r.gnavi.co.jp/plan/8jd6kjvd0000/plan-reserve/plan/plan_list/" target="_blank" title="6年4組:天神分校:WEB予約">WEB予約</a></span></li>
<li><span class="yoyaku"><a href="http://r.gnavi.co.jp/plan/ga13000/plan-reserve/plan/plan_list/" target="_blank" title="6年4組:渋谷第一、第分校:WEB予約">WEB予約</a></span></li>
<li><span class="yoyaku"><a href="http://r.gnavi.co.jp/plan/r3gxt03b0000/plan-reserve/plan/plan_list/" target="_blank" title="6年4組:新宿東口前分校:WEB予約">WEB予約</a></span></li>


        <li><a href="<?php echo home_url('/kawaramati/'); ?>">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/shop/shop07.jpg" width="174" height="174" alt="個室家座香屋 6年4組:四条河原町分校"></a><br>
          <span class="map">
          <a href="https://goo.gl/maps/ModPTrqKAPG2" target="_blank">地図</a>
          </span></li>
<li><span class="yoyaku"><a href="http://r.gnavi.co.jp/plan/k495114/plan-reserve/plan/plan_list/" target="_blank" title="6年4組:河原町分校:WEB予約">WEB予約</a></span></li>


      </ul>

      <p class="center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/info.jpg" width="579" height="150" alt="ランドセルをしょって撮影しよう!"></p>
    </section>
  </div>
  <p id="greeting"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/greeting.png" width="992" height="684" alt="asdf"></p>
</div>
</section>
<?php get_footer(); ?>