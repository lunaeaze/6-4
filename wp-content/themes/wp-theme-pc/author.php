<?php get_header(); ?>
			<section id="contents">
				<div class="contents-inner">
					<nav class="category-list cf">
						<div class="inner">

<?php get_template_part('list', 'category'); ?>

						</div>
					</nav><!--/.category-list cf-->

					<h2 class="authorTitle"><?php wp_title(''); ?>の投稿一覧</h2>
<?php get_template_part('loop', 'content'); ?>

<?php if(function_exists('wp_pagenavi')){ wp_pagenavi(); } ?>

					</div><!--/.inner-->
				</section><!--/#contents-->
				
<?php get_sidebar(); ?>
<?php get_footer(); ?>