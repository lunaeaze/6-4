<?php

/**
 * Template Name: 新宿 テンプレート
 * Created by PhpStorm.
 * User: Serinappa
 * Date: 2018/11/24
 * Time: 21:00
 *
 */
get_header('school'); ?>
    <!--
    =================
    TOP-CONTENTS
    =================-->
    <div class="shinjuku home">
        <div id="HeadTxt" class="pc">
            <h1 class="fs03 col1">新宿でお誕生日会・女子会には学校シチュエーション個室居酒屋 6年4組</h1>
        </div>
        <!--HEADER-->
        <div class="pc">
            <section id="top_cont">
                <header>
                    <div id="head_top" class="clearfix">
                        <p class="main_logo floL"><a href="<?php echo get_home_url()?>/shinjuku"><img
                                        src="<?php echo get_template_directory_uri() ?>/image/common/logo.png"
                                        width="195"
                                        height="239"
                                        alt="個室居酒屋 6年4組"></a></p>
                        <p class="shop_name floL bold">学校居酒屋 6年4組<br>
                            <span class="s_name">新宿東口駅前分校</span></p>
                        <p class="tel floL fs10 bold">03-5292-0640</p>
                    </div>
                    <p id="contact_btn"><a href="http://www.6nen4kumi.com/contact/" target="_blank"><img
                                    src="<?php echo get_template_directory_uri() ?>/image/common/contact.png"
                                    width="238"
                                    height="110"
                                    alt="学校居酒屋 6年4組 新宿東口駅前分校:お問い合わせ"></a></p>
                    <nav class="clear">
                        <ul class="navi floR col1 fs07">
                            <!--<li><a href="./">ホーム</a></li>-->
                            <li><a href="<?php echo get_home_url()?>/shinjuku/about">6年4組って？</a></li>
                            <li><a href="<?php echo get_home_url()?>/shinjuku/menu">アラカルト</a></li>
                            <li><a href="<?php echo get_home_url()?>/shinjuku/party">コース</a></li>
                            <li><a href="<?php echo get_home_url()?>/shinjuku/coupon">クーポン</a></li>
                            <li><a href="<?php echo get_home_url()?>/shinjuku/access">アクセス</a></li>
                            <li><a href="<?php echo get_home_url()?>/shinjuku/room">教室一覧</a></li>
                        </ul>
                    </nav>
                </header>
            </section>
        </div>


        <div class="sp">
            <header>
                <div><a href="<?php echo get_home_url()?>/shinjuku"><img src="<?php echo get_template_directory_uri() ?>/image/shinjuku/sp/haeder.jpg"
                                           alt="新宿でお誕生日会・女子会には学校シチュエーション個室居酒屋  6年4組"></a></div>
                <p id="panel-btn"><img src="<?php echo get_template_directory_uri() ?>/image/shinjuku/sp/pull_menu.png"
                                       alt="メニュー"></p>
                <nav id="panel">
                    <ul>
                        <li><a href="<?php echo get_home_url()?>/shinjuku/access">アクセス</a></li>
                        <li><a href="<?php echo get_home_url()?>/shinjuku/about">6年4組の魅力</a></li>
                        <li><a href="<?php echo get_home_url()?>/shinjuku/menu">アラカルト</a></li>
                        <li><a href="<?php echo get_home_url()?>/shinjuku/party">コース</a></li>
                        <li><a href="<?php echo get_home_url()?>/shinjuku/coupon">クーポン</a></li>
                        <li><a href="<?php echo get_home_url()?>/shinjuku/room">教室（個室）一覧</a></li>
                        <li><a href="http://www.6nen4kumi.com/shop/">分校（お店）一覧</a></li>
                        <li><a href="tel:0352920640"
                               onclick="ga('send', 'event', 'smartphone', 'phone-number-tap', 'shinjuku');">電話予約</a></li>
                        <li><a href="https://www.hotpepper.jp/strJ001153861/yoyaku/hpds/">WEB予約</a></li>
                        <li><a href="http://www.6nen4kumi.com/">6年4組総合TOP</a></li>
                    </ul>
                    <div id="page_x"><img
                                src="<?php echo get_template_directory_uri() ?>/image/shinjuku/sp/pull_xx_btn.jpg"
                                alt="閉じる"></div>
                </nav>
            </header>
        </div>


        <section id="main_v">
            <div class="sp">

                <!--SLIDER-->
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <img class="swiper-slide" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide1.jpg" />
                        <img class="swiper-slide" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide2.jpg" />
                        <img class="swiper-slide" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide3.jpg" />
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination swiper-pagination-white"></div>
                </div>
            </div>
            <p class="main_txt col1">各線新宿駅東口徒歩2分。総席数 90席 ★17:00～23:30（ラストオーダー　23:00）<br>
                店内には、黒板にランドセル、校長室、音楽室、理科室、保険室とあの懐かしい小学校そのものです♪<br>
                全席個室2名様～45名様まで！どの教室になるかは来校してからのお楽しみ♪椅子に座って勉強机で給食を食べて、<br>
                友だちとワイワイしたら、もう小学校時代にタイムスリップした気分になれます！プロジェクターも利用できます！</p>
        </section>
        <!--CONTENTS-->
        <section id="contents">
            <div id="main_cont" class="clearfix">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <?php the_content(); ?>

                <?php endwhile; endif; ?>
            </div>

            <?php
            get_template_part('/view/common/link_list');
            ?>
            <?php
            get_template_part('/view/footer/link_btns');
            ?>

        </section>


    </div>


<?php get_footer('school'); ?>