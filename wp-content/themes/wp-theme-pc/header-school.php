<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1, user-scalable=yes">

	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

    <meta name="robots" content="index,follow" />
    <meta name="description" content="誰もがあの頃の懐かしい気持ちになる、小学校のシチュエーションを完全再現した6年4組。 校長室、職員室など、各コンセプトに沿ってつくられた部屋は学校の雰囲気そのもの。 揚げパンやソフト麺などの「なつかしの給食メニュー」を楽しめ、参加者全員が受ける「ぬきうちテスト」は盛り上がります！" />
    <meta name="keywords" content="個室居酒屋、お誕生日会、女子会、宴会、給食、学校" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/common.css"  />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/style.css"  />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/swiper.min.css">
    <style></style>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
    <![endif]-->
    <!-- BEGIN GADWP v4.8.2.1 Universal Tracking - https://deconf.com/google-analytics-dashboard-wordpress/ -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-68571849-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- END GADWP Universal Tracking -->

    <!--アナリティクス-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-68571849-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!--アナリティクス-->

    <meta name="google-site-verification" content="kS4SPvtR1BDXbUcrBU32mubLi648RwE3eooQUNe8v2Y" />

<?php wp_head(); ?>

<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M79SJW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M79SJW');</script>
<!-- End Google Tag Manager -->
<!--
=================
HEAD-CONTENTS
=================-->

