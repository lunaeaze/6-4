<?php get_header(); ?>
<!--
=================
TOP-CONTENTS
=================-->
<?php
  $parent_id = $post->post_parent; // 親ページのIDを取得
  $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
//  echo $parent_slug; // 親ページのスラッグを表示
//  $parent_title = get_post($parent_id)->post_title; // 親ページのタイトルを取得
//  echo $parent_title; // 親ページのタイトルを表示
//  echo get_permalink($parent_id);
  ?>

<!--HEADER-->
<section id="<?php $page = get_page(get_the_ID()); echo $page->post_name;?>_top_cont" class="<?php echo $parent_slug ?>">

	<?php get_template_part('parts', 'header'); ?>

</section>

<!--CONTENTS-->
<section id="contents">
  <div id="main_cont" class="clearfix"> 

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

  <?php the_content(); ?>

<?php endwhile; endif; ?>

  </div>
</section>

<?php get_footer(); ?>