<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1, user-scalable=yes">

	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<meta name="description" content="">
	<meta name="keywords" content="">

	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/common.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/swiper.min.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

	<!--[if lt IE 9]>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/html5shiv.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]-->
	<!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
	<![endif]-->
	<!--[if lt IE 7]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
	<![endif]-->

<?php wp_head(); ?>

<!--アナリティクス-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68571849-1', 'auto');
  ga('send', 'pageview');

</script>
<!--アナリティクス-->

<meta name="google-site-verification" content="kS4SPvtR1BDXbUcrBU32mubLi648RwE3eooQUNe8v2Y" />

</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M79SJW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M79SJW');</script>
<!-- End Google Tag Manager -->
<div id="HeadTxt" class="pc">
<h1 class="fs03 col1"><?php bloginfo('name'); ?></h1>
</div>
<div id="HeadTxt" class="sp">
<header>
    <div><a href="<?php echo get_home_url()?>/"><img src="<?php echo get_template_directory_uri()?>/img/sp/header.jpg"></a></div>
    <p id="panel-btn"><img src="<?php echo get_template_directory_uri()?>/img/sp/pull_menu.png" alt="メニュー"></p>
    <nav id="panel">
        <ul>
            <li><a href="<?php echo get_home_url()?>/about/">6年4組の魅力</a></li>
            <li><a href="<?php echo get_home_url()?>/news/">お知らせ</a></li>
            <li><a href="<?php echo get_home_url()?>/menu/">メニュー</a></li>
            <li><a href="<?php echo get_home_url()?>/shop/">分校（お店）一覧</a></li>
            <li><a href="<?php echo get_home_url()?>/party/">パーティ</a></li>
            <li><a href="<?php echo get_home_url()?>/photo/">アルバム</a></li>
            <li><a href="<?php echo get_home_url()?>/info/384">先生募集</a></li>
        </ul>
        <div id="page_x"><img src="<?php echo get_template_directory_uri()?>/img/sp/pull_xx_btn.jpg" alt="閉じる"></div>
    </nav>
</header>
</div>
