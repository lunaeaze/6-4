<?php

/**
 * Template Name: 梅田店 テンプレート
 * Created by PhpStorm.
 * User: Serinappa
 * Date: 2018/11/24
 * Time: 21:00
 *
 */
get_header('school'); ?>
    <!--
    =================
    TOP-CONTENTS
    =================-->
    <div class="umeda home">
        <div id="HeadTxt" class="pc">
            <h1 class="fs03 col1">大阪　梅田でお誕生日会・女子会には学校シチュエーション個室居酒屋 6年4組</h1>
        </div>
        <!--HEADER-->
        <div class="pc">
            <section id="top_cont">
                <header>
                    <div id="head_top" class="clearfix">
                        <p class="main_logo floL"><a href="<?php echo get_home_url()?>/umeda"><img
                                        src="<?php echo get_template_directory_uri() ?>/image/common/logo.png"
                                        width="195"
                                        height="239"
                                        alt="個室居酒屋 6年4組"></a></p>
                        <p class="shop_name floL bold">学校居酒屋 6年4組<br>
                            <span class="s_name" id="name_umeda">梅田分校</span></p>
                        <p class="tel floL fs10 bold">06-6345-0604 </p>
                    </div>
                    <p id="contact_btn"><a href="http://www.6nen4kumi.com/contact/" target="_blank"><img
                                    src="<?php echo get_template_directory_uri() ?>/image/common/contact.png"
                                    width="238" height="110"
                                    alt="学校居酒屋 6年4組 梅田分校:お問い合わせ"></a></p>
                    <nav class="clear">
                        <ul class="navi floR col1 fs07">
                            <!--<li><a href="./">ホーム</a></li>-->
                            <li><a href="<?php echo get_home_url()?>/umeda/about">6年4組って？</a></li>
                            <li><a href="<?php echo get_home_url()?>/umeda/menu">アラカルト</a></li>
                            <li><a href="<?php echo get_home_url()?>/umeda/party">コース</a></li>
                            <li><a href="<?php echo get_home_url()?>/umeda/coupon">クーポン</a></li>
                            <li><a href="<?php echo get_home_url()?>/umeda/access">アクセス</a></li>
                            <li><a href="<?php echo get_home_url()?>/umeda/room">教室一覧</a></li>
                        </ul>
                    </nav>
                </header>
            </section>
        </div>


        <div class="sp">
            <header>
                <div><a href="<?php echo get_home_url()?>/umeda"><img src="<?php echo get_template_directory_uri() ?>/image/umeda/sp/haeder.jpg"
                                           alt="大阪　梅田でお誕生日会・女子会には学校シチュエーション個室居酒屋  6年4組"></a></div>
                <p id="panel-btn"><img src="<?php echo get_template_directory_uri() ?>/image/umeda/sp/pull_menu.png"
                                       alt="メニュー"></p>
                <nav id="panel">
                    <ul>
                        <li><a href="<?php echo get_home_url()?>/umeda/access">アクセス</a></li>
                        <li><a href="<?php echo get_home_url()?>/umeda/about">6年4組の魅力</a></li>
                        <li><a href="<?php echo get_home_url()?>/umeda/menu">アラカルト</a></li>
                        <li><a href="<?php echo get_home_url()?>/umeda/party">コース</a></li>
                        <li><a href="<?php echo get_home_url()?>/umeda/coupon">クーポン</a></li>
                        <li><a href="<?php echo get_home_url()?>/umeda/room">教室（個室）一覧</a></li>
                        <li><a href="<?php echo get_home_url()?>/shop/">分校（お店）一覧</a></li>
                        <li><a href="tel:0663450604"
                               onclick="ga('send', 'event', 'smartphone', 'phone-number-tap', 'umeda');">電話予約</a></li>
                        <li><a href="https://www.hotpepper.jp/strJ001153861/yoyaku/hpds/">WEB予約</a></li>
                        <li><a href="<?php echo get_home_url()?>">6年4組総合TOP</a></li>
                    </ul>
                    <div id="page_x"><img
                                src="<?php echo get_template_directory_uri() ?>/image/umeda/sp/pull_xx_btn.jpg"
                                alt="閉じる"></div>
                </nav>
            </header>
        </div>
        <section id="main_v">
            <div class="sp">

                <!--SLIDER-->
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <img class="swiper-slide" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide1.jpg" />
                        <img class="swiper-slide" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide2.jpg" />
                        <img class="swiper-slide" src="<?php echo get_stylesheet_directory_uri(); ?>/image/index/slide/slide3.jpg" />
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination swiper-pagination-white"></div>
                </div>
            </div>
            <p class="main_txt col1">梅田駅徒歩5分。総席数 88席 ★17:00～23:00（ラストオーダー　22:30）<br>
                梅田の高層ビル33Fにあるお酒が飲めるユニークな「小学校」。それが「6年4組 梅田分校」です。
                お店には「教室」と呼ばれる個室が12部屋。宴会におすすめの「大教室」もあります。
                話題に事欠かないものばかりで盛り上がります。お店のスタッフは「学校の先生」。お客様は「生徒」です。
                女子会・お誕生日コースなど定番のコースに加えて、季節のイベントに合わせた月替わりのコースもご用意しています。</p>
        </section>
        <!--CONTENTS-->
        <section id="contents">
            <div id="main_cont" class="clearfix">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <?php the_content(); ?>

                <?php endwhile; endif; ?>

            </div>
            <?php
            get_template_part('/view/common/link_list');
            ?>
            <?php
            get_template_part('/view/footer/umeda/link_btns');
            ?>

        </section>


    </div>


<?php get_footer('school'); ?>