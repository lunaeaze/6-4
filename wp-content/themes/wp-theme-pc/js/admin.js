/**
 * Created by Serinappa on 2017/09/13.
 */
jQuery(function ($) {

    window.____original_send_to_editor = window.send_to_editor;

    var $body = $(document.body)

    $body.on("click", "#add_course", function (e) {
        e.preventDefault()
        $(".course_update_field").addClass("active");
        $(".course_update_field").find("input,textarea").filter(":not([type=checkbox])").val("");
        $(".course_update_field").find("img").attr("src","");
        var next_idx = $("#next_idx").val()
        $("#update_idx").val(next_idx);
    })

    $body.on("click", "textarea,input,button", function (e) {
        e.stopPropagation()
    })

    $body.on("click", "tr", function (e) {
        var $this = $(this)
        if($this.find("th").length)return;
        if (!$this.hasClass("active")) {
            $("tr").removeClass("active");
            $(".course_update_field").addClass("active");
            $this.addClass("active")
            // フィールドを更新
            var $fieldTarget  = $(this);
            var $course_update_field = $(".course_update_field");
            var count = $fieldTarget.attr("data-create-count");
            var list = ["c_title","c_subtitle","c_image","c_price","c_a_count","c_h_count","c_p_detail","c_detail","c_menu","c_d_time"]
            for(let i = 0 , len =list.length ;i<len;i++){
                var param = list[i];
                $course_update_field.find("[name='"+param+"']").val($fieldTarget.find("[name^='"+param+"']").val());
            }
            $course_update_field.find("[name='c_act']").prop("checked",$fieldTarget.find("._active_ck").prop("checked"));
            $course_update_field.find("#update_img").attr("src",$fieldTarget.find("[name^='c_image']").val());
            $("#update_idx").val(count);
        }
    });
    $body.on("click",'[name="del"]',function(){
        var $fieldTarget  = $(this).parents("tr");
        var count = $fieldTarget.attr("data-create-count");
        $("#update_idx").val(count);
        $('[name="course_update_type"]').val("del")
    });


    var formfield = ""
    //メタボックス内のボタンからmedia_upload.phpを呼び出す
    $('#update_image_load').each(function (i) {
        $(this).click(function () {
            // thickboxの関数
            tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
            formfield = true;
            return false;
        });
    });

    // 削除ボタン
    $(".del_btn").on("click", function () {
        var $this = $(this);
        var $parentImageBox = $this.parents(".image_input_item");
        var $inputTarget = $parentImageBox.find('[type="hidden"]');
        $inputTarget.val("");
        $parentImageBox.find("img").attr("src", "");

    })
    // コースクリ九字
    $(".del_btn").on("click", function () {
        var $this = $(this);
        var $parentImageBox = $this.parents(".image_input_item");
        var $inputTarget = $parentImageBox.find('[type="hidden"]');
        $inputTarget.val("");
        $parentImageBox.find("img").attr("src", "");

    })

    //メディアアップローダーからきた変数htmlを各々へ挿入
    window.send_to_editor = function (html) {
        if (formfield) {
            var fileurl = $(html).find("img").attr('src');
            $('#update_img_val').val(fileurl);
            tb_remove();
            // 挿入した画像プレビューさせるエリアへソースをいれる
            $('#update_img').attr('src', fileurl);
        } else {
            window.____original_send_to_editor(html);
        }
        formfield = false;
    };
    //window.original_send_to_editor = window.send_to_editor;
});