<?php get_header(); ?>

	<section id="main_cwrap">
		<div id="main_c">

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<p class="pankuzu"><a href="<?php echo home_url('/'); ?>">TOP</a> > <a href="<?php echo home_url('/news/'); ?>">最新記事</a> > <?php the_title(); ?></p>

	    <div class="post">
		    <p class="post_day"><?php the_time('Y年m月d日'); ?></p>
		    <p class="post_title"><?php the_title(); ?></p>
		    <?php the_content(); ?>
	    </div>

<?php endwhile; endif; ?>

<?php query_posts('post_type=post&posts_per_page=-1'); ?>
	<?php if(have_posts()): while(have_posts()): the_post(); ?>
		<?php if(isLast()): $lasturl = get_the_permalink(); endif; ?>
		<?php if(isFirst()): $firsturl = get_the_permalink(); endif; ?>
	<?php endwhile; endif; ?>
<?php wp_reset_query(); ?>

	    <ul class="pagenation clearfix">
		    <li><a href="<?php echo $firsturl; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/first_av.png" alt="最初の記事へ"></a></li>
		    <li><?php next_post_link('%link','<img src="http://6-4.sakura.ne.jp/wp-content/themes/wp-theme-sp/img/prev_av.png" alt="前のページへ">') ?></li>
		    <li><?php previous_post_link('%link','<img src="http://6-4.sakura.ne.jp/wp-content/themes/wp-theme-sp/img/next_av.png" alt="次のページへ">') ?></li>
		    <li><a href="<?php echo $lasturl; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/last_av.png" alt="最後のページへ"></a></li>
	    </ul>

	<?php get_template_part('parts', 'bottom'); ?>

		</div>
	</section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>