<?php get_header(); ?>

	<section id="main_cwrap">
		<div id="main_c">

			<p class="bderb title2">お知らせ</p>

<?php query_posts('paged='.$paged.'&post_type=post'); ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>


	    <div class="post">
		    <p class="post_day"><?php the_time('Y年m月d日'); ?></p>
		    <p class="post_title"><?php the_title(); ?></p>
		    <?php the_content(); ?>
	    </div>

<?php endwhile; endif; ?>

	    <?php if(function_exists('wp_pagenavi')){ wp_pagenavi(); } ?>

<?php wp_reset_query(); ?>

	<?php get_template_part('parts', 'bottom'); ?>
	
		</div>
	</section>


	
<?php get_sidebar(); ?>
  
<?php get_footer(); ?>