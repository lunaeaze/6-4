<?php get_header(); ?>

	<section id="main_cwrap">
		<div id="main_c">

			<p class="pankuzu"><a href="<?php echo home_url('/'); ?>">TOP</a> > <a href="<?php echo home_url('/news/'); ?>">最新記事</a> > <?php wp_title(''); ?></p>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	    <div class="post">
		    <p class="post_day"><?php the_time('Y年m月d日'); ?></p>
		    <p class="post_title"><?php the_title(); ?></p>
		    <?php the_content(); ?>
	    </div>

<?php endwhile; endif; ?>

			<?php if(function_exists('wp_pagenavi')){ wp_pagenavi(); } ?>

	<?php get_template_part('parts', 'bottom'); ?>
	
		</div>
	</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>