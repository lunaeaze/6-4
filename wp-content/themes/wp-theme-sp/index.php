<?php get_header(); 
							$post_cat = get_the_category();
							$post_cat_name = $post_cat[0]->name;
							$post_cat_id = $post_cat[0]->cat_ID;
							foreach($post_cat as $v){
								if($v->parent != 0){
									$post_cat_name = $v->name;
									$post_cat_id = $v->cat_ID;
								}
							}
							$color = get_field('color','category_'.$post_cat_id);
?>

<div class="nav">
  <ul class="clearfix">
    <li><a href="<?php echo home_url('/about/'); ?>">6年4組って</a></li>
    <li><a href="<?php echo home_url('/news/'); ?>">お知らせ</a></li>
    <li><a href="<?php echo home_url('/menu/'); ?>">メニュー</a></li>
    <li><a href="<?php echo home_url('/shop/'); ?>">教室一覧</a></li>
    <li><a href="<?php echo home_url('/party/'); ?>">パーティ</a></li>
    <li><a href="<?php echo home_url('/photo/'); ?>">アルバム</a></li>
  </ul>
</div>
<section id="main_cwrap">
  <div id="main_c">
    <p class="clip"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/clip.png" width="39" height="87"></p>
    <p class="title2 center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/class_title.png" width="166" height="29"></p>
    <ul class="clearfix room">
      <li><a href="http://www.6nen4kumi.com/umeda/"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/umeda_l.jpg" width="173" height="173" alt="個室家座香屋 6年4組:梅田分校"></a></li>
      <li><a href="http://www.6nen4kumi.com/tennoji/"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/abeno_l.jpg" width="172" height="173" alt="個室家座香屋 6年4組:阿倍野天王寺駅前分校"></a></li>
      <li><a href="http://www.6nen4kumi.com/tenjin/"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tenjin_l.jpg" width="174" height="173" alt="個室家座香屋 6年4組:天神分校"></a></li>
      <li class="y-outer_r"> <span class="yoyaku r"> <a href="https://www.hotpepper.jp/strJ000717096/yoyaku/hpds/" target="_blank" title="6年4組:梅田分校:WEB予約">WEB予約</a></span></li>
      <li class="y-outer_b"> <span class="yoyaku b"> <a href="https://www.hotpepper.jp/strJ001099992/yoyaku/hpds/" target="_blank" title="6年4組:阿倍野天王寺駅前分校:WEB予約">WEB予約</a></span> </li>
      <li class="y-outer_g"> <span class="yoyaku g"> <a href="https://www.hotpepper.jp/strJ000762080/yoyaku/hpds/" target="_blank" title="6年4組:天神分校:WEB予約">WEB予約</a></span></li>
      <li><a href="http://www.6nen4kumi.com/shibuya/"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sibuya1_l.jpg" width="173" height="173" alt="個室家座香屋 6年4組:渋谷第一分校"></a></li>
      <li><a href="http://www.6nen4kumi.com/shinjuku/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sinjyuku_e_l.jpg" width="173" height="173" alt="個室家座香屋 6年4組:新宿東口駅前分校"></a></li><li><a href="http://www.6nen4kumi.com/ikebukuro/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ikebukuro_l.jpg" width="174" height="173" alt="個室家座香屋 6年4組:池袋分校"></a></li><li class="y-outer_b"> <span class="yoyaku b"> <a href="https://www.hotpepper.jp/strJ000768666/yoyaku/hpds/" target="_blank" title="6年4組:渋谷第一、第分校:WEB予約">WEB予約</a></span></li><li class="y-outer_g"> <span class="yoyaku g"> <a href="https://www.hotpepper.jp/strJ001108736/yoyaku/hpds/" target="_blank" title="6年4組:新宿東口前分校:WEB予約">WEB予約</a></span> </li><li class="y-outer_r"> <span class="yoyaku r"> <a href="https://www.hotpepper.jp/strJ001153861/yoyaku/hpds/" target="_blank" title="6年4組:池袋東口分校:WEB予約">WEB予約</a></span></li><li><a href="http://www.6nen4kumi.com/nagoya/"> <img src="http://www.6nen4kumi.com/shinjuku/image/index/shop/shop-nagoya.jpg" width="174" height="174" alt="個室家座香屋 6年4組:名古屋名駅分校"></a></li><li><a href="http://www.6nen4kumi.com/sakae/"> <img src="http://www.6nen4kumi.com/wp-content/themes/wp-theme-pc/image/index/shop/shop-sakae.jpg" width="174" height="174" alt="個室家座香屋 6年4組:栄分教場"></a></li>
    </ul>
    <ul class="clearfix room">
		<li class="y-outer_g"> <span class="yoyaku g"> <a href="https://www.hotpepper.jp/strJ001192959/yoyaku/hpds/" target="_blank" title="6年4組:名古屋名駅分校:WEB予約">WEB予約</a></span> </li>
    <ul class="clearfix room">
		<li class="y-outer_g"> <span class="yoyaku b"> <a href="https://www.hotpepper.jp/strJ001202008/yoyaku/hpds/" target="_blank" title="6年4組:sakae分教場:WEB予約">WEB予約</a></span> </li>
	</ul>
    <p class="title2 center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/news_title.png" width="122" height="23"></p>
    <table>
      <?php query_posts('cat=1&posts_par_page=3'); if(have_posts()): while(have_posts()): the_post(); ?>
      <tr>
        <th>
          <?php the_time('Y.m.d'); ?>
        </th>
        <td><a href="<?php the_permalink(); ?>">
          <?php the_title(); ?>
          </a></td>
      </tr>
      <?php endwhile; endif; ?>
    </table>
    <p class="title2 center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/menu_title.png" width="122" height="23"></p>
    <table>
      <?php query_posts('cat=7&posts_par_page=3'); if(have_posts()): while(have_posts()): the_post(); ?>
      <tr>
        <th>
          <?php the_time('Y.m.d'); ?>
        </th>
        <td><a href="<?php the_permalink(); ?>">
          <?php the_title(); ?>
          </a></td>
      </tr>
      <?php endwhile; endif; ?>
    </table>
    <p class="title2 center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/media_title.png" width="122" height="23"></p>
    <table>
      <?php query_posts('cat=2&posts_par_page=3'); if(have_posts()): while(have_posts()): the_post(); ?>
      <tr>
        <th>
          <?php the_time('Y.m.d'); ?>
        </th>
        <td><a href="<?php the_permalink(); ?>">
          <?php the_title(); ?>
          </a></td>
      </tr>
      <?php endwhile; endif; ?>
    </table>
    <!--p class="rand"><a href="http://www.6nen4kumi.com/info/335/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/randsel.jpg" width="640" alt
="★あんハピ♪コラボ中!!!★"></a></p-->
    <!--p class="rand"><a href="http://r.gnavi.co.jp/seg3v6m50000/" target="_blank"><img style="border:5px solid #fff;" src="<?php echo get_stylesheet_directory_uri(); ?>/img/343sp.jpg" width="640" height="auto" alt="元祖343鮨 銀座本店"></a></p-->
    <p class="rand"><a href="http://www.6nen4kumi.com/info/646/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/dagashi_sp.png" width="640"></a></p>
    <p class="rand"><a href="http://www.6nen4kumi.com/contact/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tv.jpg" width="640"></a></p>
    <p class="rand"><a href="http://www.6nen4kumi.com/contact/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cora.jpg" width="640"></a></p>
    <p class="rand"><a href="http://www.6nen4kumi.com/info/384/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rec.jpg" width="640"></a></p>
    <p class="rand"><iframe width="80%" height="420" src="https://www.youtube.com/embed/fJG68ypGEeg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></p>
  </div>
</section>
<div class="contwrap clearfix">
  <h2>6年4組は小学校をコンセプトとしています。</h2>
  <p>あの頃の懐かしい気持ちになる、小学校のシチュエーションを完全再現した店内や、揚げパンやソフト麺など懐かしの給食メニューの数々。お店のスタッフは「学校の先生」お客様は「生徒」です。スタッフ全員、先生らしく接客いたします。名物は、参加者全員が受ける「抜き打ちテスト」です。勉強も忘れずに♪ぜひご来校ください。</p>
</div>
<p class="txt_box"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/r18_sp.png"></p>
	<div class="vrwrap clearfix">
		<h2>VRで見える!6年4組</h2>
		<p><a herf="<?php echo get_stylesheet_directory_uri(); ?>/image/index/vr-64.jpg" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/vr-64.jpg" width="100%" height="auto" alt="VRで見える!6年4組"></a></p>
		<p>今すぐ小学校のあのころ体験！画像クリックしてVRゴーグルで覗いてみよう!</p>
	</div>
<p class="txt_box"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/aisatu.png" width="723" height="981"></p>
<div class="contwrap clearfix" id="notice-copy">
<h2>ご注意</h2>
<p>最近「家座香屋6年4組」に似たお店が数多くあります。阪神食品株式会社の運営する学校居酒屋は「家座香屋6年4組」のみです。誤解されたお客様には大変申し訳ございません。「家座香屋6年4組」には商標登録・実用新案の飲食店です。本物をご利用ください！</p>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
