<?php get_header(); ?>

	<section id="main_cwrap">
		<div id="main_c">

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php if(get_field('sp-editor')) : ?><?php the_field('sp-editor', $post->ID); ?><?php endif; ?>

<?php endwhile; endif; ?>

		</div>
	</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>