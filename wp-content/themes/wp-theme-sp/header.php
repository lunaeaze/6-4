<!DOCTYPE HTML>
<html lang="ja">

<!--================== ↓head↓ ==================-->

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=750px">
<meta name="format-detection" content="telephone=no">
<title>
<?php wp_title('&laquo;', true, 'right'); ?>
<?php bloginfo('name'); ?>
</title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/jquery.bxslider.css">
<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
<!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<![endif]-->
<!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
	<![endif]-->
<!--[if lt IE 7]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
	<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/selectivizr.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/pull.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/Pc2Sp.js"></script>
<?php wp_head(); ?>

<!--アナリティクス-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68571849-1', 'auto');
  ga('send', 'pageview');

</script>
<!--アナリティクス-->

</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T68H98"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T68H98');</script>
<!-- End Google Tag Manager -->
<script>
	$(document).ready(function(){
  $('.bxslider').bxSlider({
  	auto : "ture",
  });
});
</script>

<script>
$.switchPc2Sp({
	mode : "sp" ,
	anchorToPcInSp : "#anchorToPcInSp" 
});
</script>
<header>
  <h1><a href="<?php echo home_url('/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/header.jpg" width="750" height="313"></a></h1>
  <p id="panel-btn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pull_menu.png" width="73" height="64" alt="メニュー"></p>
  <nav id="panel">
    <ul>
      <li><a href="<?php echo home_url('/about/'); ?>">6年4組の魅力</a></li>
      <li><a href="<?php echo home_url('/news/'); ?>">お知らせ</a></li>
      <li><a href="<?php echo home_url('/menu/'); ?>">メニュー</a></li>
      <li><a href="<?php echo home_url('/shop/'); ?>">分校（お店）一覧</a></li>
      <li><a href="<?php echo home_url('/party/'); ?>">パーティ</a></li>
      <li><a href="<?php echo home_url('/photo/'); ?>">アルバム</a></li>
      <li><a href="<?php echo home_url('/info/384'); ?>">先生募集</a></li>
    </ul>
    <div id="page_x"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/pull_xx_btn.jpg" width="750" height="127" alt="閉じる"></div>
  </nav>
</header>
<?php if(is_home()): ?>
<div id="mainvisual">
  <div id="slider">
    <ul class="bxslider">
      <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/slider01.jpg" width="748" height="375"></li>
      <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/slider02.jpg" width="748" height="375"></li>
      <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/slider03.jpg" width="748" height="375"></li>
    </ul>
  </div>
  <div id="bner">
    <ul id="bner_li">
            <li><a href="<?php echo home_url('/about/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/main_img01.png" width="241" height="148"></a></li>
      <!--<li><a href="http://www.6nen4kumi.com/bounenkai/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/main_img01_bounenkai.png" width="241" height="148" alt
="忘年会"></a></li>-->
      <li><a href="<?php echo home_url('/menu/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/main_img02.png" width="242" height="148"></a></li>
      <li><a href="<?php echo home_url('/party/'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/main_img03.png" width="240" height="148"></a></li>
    </ul>
  </div>
</div>
<!--mainvisual-->

<?php elseif(is_page()): ?>
<div id="mainvisual"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/<?php $page = get_page(get_the_ID()); echo $page->post_name;?>_main.jpg" alt="<?php echo get_the_title($post->post_parent); ?>"> </div>
<!--mainvisual-->

<?php endif; ?>
<article>
<div id="content">
