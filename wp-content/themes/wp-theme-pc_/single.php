<?php get_header(); ?>
<!--
=================
TOP-CONTENTS
=================--> 
<!--HEADER-->
<section id="a_top_cont">

	<?php get_template_part('parts', 'header'); ?>

</section>

<!--CONTENTS-->
<section id="contents">
  <div id="main_cont" class="clearfix"> 
    <p id="pin"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/common/pin.png" width="42" height="80" alt="pin"></p>

<?php get_sidebar(); ?>
    
    <!--MAIN_CONT-->
    <section id="main_r_cont">

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	    <p class="pankuzu fs02"><a href="<?php echo home_url('/'); ?>">TOP</a> > <a href="<?php echo home_url('/news/'); ?>">最新記事</a> > <?php the_title(); ?></p>
    
	    <div class="post">
		    <p class="post_day fs03"><?php the_time('Y年m月d日'); ?></p>
		    <p class="post_title fs07 bold"><?php the_title(); ?></p>
		    <?php the_content(); ?>
	    </div>
    
<?php endwhile; endif; ?>

<?php query_posts('post_type=post&posts_per_page=-1'); ?>
	<?php if(have_posts()): while(have_posts()): the_post(); ?>
		<?php if(isLast()): $lasturl = get_the_permalink(); endif; ?>
		<?php if(isFirst()): $firsturl = get_the_permalink(); endif; ?>
	<?php endwhile; endif; ?>
<?php wp_reset_query(); ?>

	    <ul class="pagenation">
		    <li><a href="<?php echo $firsturl; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/archive/btn/first_page_off.png" width="123" height="20" alt="最初の記事へ"></a></li>
		    <li><?php next_post_link('%link','<img src="http://6-4.sakura.ne.jp/wp-content/themes/wp-theme-pc/image/archive/btn/prev_page_off.png" width="107" height="20" alt="前のページへ">') ?></li>
		    <li><?php previous_post_link('%link','<img src="http://6-4.sakura.ne.jp/wp-content/themes/wp-theme-pc/image/archive/btn/next_page_off.png" width="107" height="20" alt="次のページへ">') ?></li>
		    <li><a href="<?php echo $lasturl; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/archive/btn/last_page_off.png" width="124" height="20" alt="最後のページへ"></a></li>
	    </ul>

    </section>


  </div>
</section>
  
<?php get_footer(); ?>