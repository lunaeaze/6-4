<?php

//アイキャッチ画像
add_theme_support('post-thumbnails');
set_post_thumbnail_size(800, 9999);

//
//不要なタグを削除
//remove_action('wp_head', 'feed_links');
//remove_action('wp_head', 'feed_links_extra');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'wp_generator');


function postlist($query) {
	if ( is_admin() || ! $query->is_main_query() )
		return;

	/*
	if ( $query->is_home() ) {
    $query->set( 'posts_per_page', '5' );
	}


	if ( $query->is_month() ) {
    $query->set( 'posts_per_page', '1' );
	}

	if ( $query->is_archive() ) {
    $query->set( 'posts_per_page', '1' );
	}
	*/

}
add_action( 'pre_get_posts', 'postlist' );

function isFirst(){
    global $wp_query;
    return ($wp_query->current_post === 0);
}

function isLast(){
    global $wp_query;
    return ($wp_query->current_post+1 === $wp_query->post_count);
}


// エディタでテンプレートパスを利用する ショートコード
function shortcode_templateurl() {
     return get_bloginfo('template_url');
}
add_shortcode('template_url', 'shortcode_templateurl');

// エディタでサイトパスを利用する ショートコード
function shortcode_siteurl() {
     return get_bloginfo('url');
}
add_shortcode('site_url', 'shortcode_siteurl');