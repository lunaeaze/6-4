<?php get_header(); ?>
<!--
=================
TOP-CONTENTS
=================--> 
<!--HEADER-->
<section id="<?php $page = get_page(get_the_ID()); echo $page->post_name;?>_top_cont">

	<?php get_template_part('parts', 'header'); ?>

</section>

<!--CONTENTS-->
<section id="contents">
  <div id="main_cont" class="clearfix"> 

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

  <?php the_content(); ?>

<?php endwhile; endif; ?>

  </div>
</section>

<?php get_footer(); ?>