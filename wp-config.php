<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

//define('WP_HOME','http://lunaeaze-test.sakura.ne.jp/6-4');
//define('WP_SITEURL','http://lunaeaze-test.sakura.ne.jp/6-4');

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
//define('DB_NAME', '6-4_wp'); // 旧サーバ
//define('DB_NAME', '6-4_wp_2019');
// define('DB_NAME', 'lunaeaze-test_6-4');
 define('DB_NAME', 'lunaeaze-test_6-4_tes');

/** MySQL データベースのユーザー名 */
//define('DB_USER', '6-4');
define('DB_USER', 'lunaeaze-test');

/** MySQL データベースのパスワード */
//define('DB_PASSWORD', 'hanshin0604');
define('DB_PASSWORD', 'akakara1209');

/** MySQL のホスト名 */
//define('DB_HOST', 'mysql516.db.sakura.ne.jp'); // 旧サーバ
//define('DB_HOST', 'localhost');
//define('DB_HOST', 'mysql642.db.sakura.ne.jp'); // 新サーバ
define('DB_HOST', 'mysql629.db.sakura.ne.jp');  // テストサーバ

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JZ61BQA&2zSeG]giWN-h!~o^-5OvM:1iv{%iI[jJ5JiCg@^{Y5t;|CF~[-4)*{gp');
define('SECURE_AUTH_KEY',  '=m1@Uj)-+g/=7`[m9&`Y-u+lV+Xcjo_maApZ#R584ZW<::Snsd`*XCktUl-!/}mh');
define('LOGGED_IN_KEY',    'J,(9#J<8~9KFX{:{)k7dI7~u%S@i--yl|$}|mp@yJX1s2>e[06%Baciob64vRcU7');
define('NONCE_KEY',        '3n -7{-%5d2F6H?||L7.aSpUl5a<NZQ3?z@$Y];.Q`eAK C{0/^;z@sP|%~)N|Y;');
define('AUTH_SALT',        '1 >CRN_oH3xO!ftX9p-UBt,++7c2pu^J[|drq#.k:=g5/8r~?mJH~N6Tf+z/]91o');
define('SECURE_AUTH_SALT', 'k,L,]7KgJEi@pNhHIL&7uF&V,nhCir9?F-AdzacQpB=T=S/G XY)_:[$hH(~wlU@');
define('LOGGED_IN_SALT',   '^iu``c$e4QY]8QOC84*Y|9<McX@x+/`/$UN|agYD4Ky$f7X Sk{lr+R#/? [p8qA');
define('NONCE_SALT',       'A ~qw+t%yBF1W2buwv*otli4U6y6 zI+lTo.pD]!gckGkLs.K1{|#7>_G@|[oGBg');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'ha_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
