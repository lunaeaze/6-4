// JavaScript Document

var gmg = new google.maps.Geocoder(),
            map, center, mk ;
        gmg.geocode({
            'address': '愛知県名古屋市中川区西日置1-1-3'//表示したい場所
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {//ジオコードが成功したかどうかチェック
                center = results[0].geometry.location;
                initialize();
                mk = new google.maps.Marker({//ここからマーカーの設定
                    map: map, position: results[0].geometry.location,
                    icon: {//マーカー画像のパスを設定
                        url: 'http://majide.co.jp/web/pinpoint_html/image/common/map_pin.png',
                        anchor: {//マーカーを表示させる場所を設定
                            x: 0,
                            y: 50
                        }},
                        title: ''//マーカー画像のtitle
                });
            }
        });

        function initialize() {//初期化
            var options = {
                center: center,
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER
                },
             styles: [
    {
        "stylers": [
            {
                "saturation": -100
            },
            {
                "gamma": 1
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.place_of_worship",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.place_of_worship",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "saturation": 50
            },
            {
                "gamma": 0
            },
            {
                "hue": "#50a5d1"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#333333"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text",
        "stylers": [
            {
                "weight": 0.5
            },
            {
                "color": "#333333"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "labels.icon",
        "stylers": [
            {
                "gamma": 1
            },
            {
                "saturation": 50
            }
        ]
    }
]//彩度
            };
            map = new google.maps.Map($("#map").get(0), options);//セレクタ内に地図取得
        }
		
	